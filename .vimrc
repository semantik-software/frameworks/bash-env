" =======================================================
" Vim Configuration
" =======================================================


" Strip trailing whitespace on save
autocmd BufWritePre * :%s/\s\+$//e

" =======================================================
" General Settings
" =======================================================
set nocompatible              " turn off vi compatibility
set undolevels=1000           " loads of undo!
set history=50                " size of :command history
set modelines=20
set modeline                  " Turn on modelines
set showcmd                   " Display incomplete commands
set showmatch                 " Show matches on parens, brackets, etc.

" set visualbell
set noerrorbells

set nobackup                  " do not keep a backup file, use versions instead
set ruler                     " show the cursor position all the time
set number                    " show line numbers


" --------------------------------------
" Configure Tabs
" --------------------------------------
set tabstop=4
set softtabstop=4
set shiftwidth=4
set shiftround            " indent/outdent to nearest tabstop
set expandtab             " enter spaces when tab is pressed

" When the page starts to scroll, keep the cursor away
" from the top and bottom.
set scrolloff=10
set sidescrolloff=15
set sidescroll=1

" --------------------------------------
" Color Scheme
" --------------------------------------
filetype on
syntax on
" Color schemes: default, murphy, slate, koehler, torte, delek
colorscheme slate
" dark or light
set background=dark

" --------------------------------------
" Set syntax on non-standard files
" --------------------------------------
au BufRead,BufNewFile *.pf set filetype=tcsh
au BufRead,BufNewFile *.sh set filetype=sh
au BufRead,BufNewFile *.md set filetype=markdown
au BufRead,BufNewFile *.xpl set filetype=perl
au BufRead,BufNewFile *.pl set filetype=perl
au BufRead,BufNewFile *.rs set filetype=rust
au BufRead,BufNewFile *.py set filetype=python
au BufRead,BufNewFile *.xpy set filetype=python
au BufRead,BufNewFile *.c set filetype=c
au BufRead,BufNewFile *.h set filetype=c
au BufRead,BufNewFile *.cpp set filetype=cpp
au BufRead,BufNewFile *.hpp set filetype=cpp

" --------------------------------------
" Set TABS for Makefiles
" --------------------------------------
au BufRead,BufNewFile Makefile setlocal noexpandtab

" Highlight tabs in leading whitespace
highlight ExtraWhitespace ctermbg=red guibg=red


" --------------------------------------
" Set the terminal title
" --------------------------------------
set title titlestring=%F

" --------------------------------------
" Statusline
" --------------------------------------
set laststatus=2

" --------------------------------------
" Search and Replace
" --------------------------------------
set noincsearch            " don't show partial matches as search is entered
set hlsearch               " highlight search patterns
set ignorecase             " ignore case distinction when searching
set smartcase              " ... unless there are capitals in the search string.
" set nowrapscan             " Don't wrap to top of buffer when searching


" ======================================
" Plugins
" ======================================
helptags ~/.vim/pack/dist/start/vim-airline/doc


" --------------------------------------
" Highlighting Leading Whitespace
" --------------------------------------
" Set background of tabs in leading whitespace to RED
highlight LeadingTabs ctermbg=red guibg=red
match LeadingTabs /^\t\+/

" Set leading whitespace to dots and tabs to arrows.
" set list
" set listchars+=tab:>-,space:·
