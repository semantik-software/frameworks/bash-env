#!/usr/bin/env bash

# Function to set color codes using tput if available.
function set_colorcodes() {
    if command -v tput >/dev/null 2>&1; then
        # Set color codes using tput
        export normal=$(tput sgr0)
        export red=$(tput setaf 1)
        export green=$(tput setaf 2)
        export yellow=$(tput setaf 3)
        export blue=$(tput setaf 4)
        export magenta=$(tput setaf 5)
        export cyan=$(tput setaf 6)
        export grey=$(tput setaf 8)
        export orange=$(tput setaf 9)
    else
        # Set color codes to empty strings if tput is not available
        export normal=''
        export red=''
        export green=''
        export yellow=''
        export blue=''
        export magenta=''
        export cyan=''
        export grey=''
        export orange=''
    fi
}
export -f set_colorcodes
set_colorcodes

# Function print message with some formatting
function message() {
    local msg=${1:-""}
    echo "${blue}>>> ${cyan}${msg}${normal}"
}
export -f message

# Function to print out an envvar
function print_envvar() {
    local envvar=${1:?}
    local value=${!envvar}
    message "${cyan}${envvar}${normal}=${magenta}${value}"
}
export -f print_envvar

# Function to execute a command and check the status
function execute_command_checked() {
    local cmd=${1:?}

    message "\$ ${magenta}${cmd}"

    if [[ -z "$DRYRUN" ]]; then
        eval "${cmd:?}"
        local status=$?
        if [ ${status:?} -eq 0 ]; then
            message "${green}OK"
        else
            message "${red}FAIL"
            exit ${status:?}
        fi
    else
        message "${orange}DRYRUN"
    fi
}
export -f execute_command_checked
