#!/usr/bin/env bash
# Load the crl-mahi venv (this script should be sourced)
scriptdir=$(dirname $BASH_SOURCE)
scriptname=$(basename $BASH_SOURCE)

# Loads one of the virtual environments based on what is available.
# The *first* in alphabetic order will be the default one.
#
# Usage:
#
# $ source qed-activate-venv.bash
#    - User is presented choices and will select the NUMBER of the
#      desired venv to use.
#
# $ SELECT_OVERRIDE=<name_of_desired_option> source qed-activate-venv.bash
#    - Script will automatically pick option 3 from the list.
#    - SELECT_OVERRIDE should be the *name* of the desired option.
#      i.e., `SELECT_OVERRIDE=crl-dev source qed-activate-venv.bash`
#

#
# Set Arguments
#

# If ARG_VENV_PREFIX is set, use it but if not, then we look for __VENVPATH__
# and if that isn't set then we default to $HOME/.venv
[ -z "${ARG_VENV_PREFIX}" ] && ARG_VENV_PREFIX=${__VENVPATH__:-${HOME}/.venv}

# TODO: Should modify this so that an envvar like VENVPATH can come in that works
# TODO: like a MODULEPATH (: delimited list of paths) and work kind of like LMOD
# TODO: or something like that.

function venv_is_active() {
    [ -d "${VIRTUAL_ENV}" ] && echo "true" || echo "false"
}

function venv_activate() {
    local VENV_PATH=${1:?}
    if [[ -e ${VENV_PATH:?} ]]; then
        echo -e "${yellow}Activate VENV: ${magenta}$(basename ${VENV_PATH})${normal}"
        source ${VENV_PATH:?}/bin/activate
    else
        echo -e "${red}Failed to activate VENV: ${magenta}$(basename ${VENV_PATH})${normal}"
    fi
}

function venv_deactivate() {
    if [[ "$(venv_is_active)" == "true" ]]; then
        echo -e "${yellow}Deactivate Loaded VENV: ${magenta}$(basename ${VIRTUAL_ENV})${normal}"
        deactivate
    fi
}

function venv_dirs() {
    local venv_path=${1}
    local venv_dirs
    if [[ -d ${venv_path:?} ]]; then
        venv_dirs=("${venv_path}"/*)
        [[ ${#venv_dirs[@]} -gt 0 ]] && echo "${venv_dirs[@]}"
    fi
}

function get_subdirectories() {
    local PREFIX=${1:?}
    shopt -s nullglob
    local SUBDIRS=( $(find "${PREFIX}" -mindepth 1 -maxdepth 1 -type d -exec basename {} \; | sort) )
    shopt -u nullglob
    echo "${SUBDIRS[@]}"
}

# Get the venv subdirs
TMP_VENVDIRS=( $(get_subdirectories ${ARG_VENV_PREFIX}) )

echo -e ""
echo -e "${yellow}Select VENV from the following list...${normal}"
echo -e "${yellow}Use envvar ${magenta}SELECT_OVERRIDE=<venv_name> ${yellow}to skip interactive selection.${normal}"
echo -e ""

# Get user selection
TMP_VENVNAME=$(select_with_default ${TMP_VENVDIRS[@]})

# Deactivate current VENV if one is loaded.
venv_deactivate

# Load the VENV
venv_activate ${ARG_VENV_PREFIX}/${TMP_VENVNAME}

# Update
python -m pip install --upgrade pip >& /dev/null


# Notify
echo "${yellow}--------------------------------------------------------------------------------${normal}"
echo "${yellow}-${normal}"
if [ -z ${VIRTUAL_ENV} ]; then
    echo "${yellow}-${normal} VENV Load: ${magenta}${TMP_VENVNAME:?} ${red}FAILED${normal}"
else
    echo "${yellow}-${normal} VENV Load: ${magenta}${TMP_VENVNAME:?} ${green}SUCCESS${normal}"
    echo "${yellow}-${normal} VENV Path: ${magenta}${VIRTUAL_ENV}${normal}"
    echo "${yellow}-${normal}"
    echo "${yellow}-${normal} Use ${magenta}$ deactivate${normal} to disable."
fi
echo "${yellow}-${normal}"
echo "${yellow}--------------------------------------------------------------------------------${normal}"


# Cleanup Temp Vars
unset TMP_QED_VENVDIRS
unset TMP_QED_VENVNAME

unset ARG_VENV_PREFIX
