#!/usr/bin/env bash
#

# Python Version Information
vermajor=3
verminor=8
verpatch=18
pyversion="${vermajor}.${verminor}.${verpatch}"

# Check requirements
if [ "$(uname)" != "Darwin" ]; then
    echo "This script requires OSX / Darwin"
    exit 1
fi
if [ $(command -v brew) == "" ]; then
    echo "This script requires Homebrew"
    exit 1
fi

# Cleanup
[ -f Python-${pyversion}.tgz ] && rm Python-${pyversion}.tgz

# Install Homebrew Dependencies
brew_deps=(
    gcc
    cmake
)

execute_command "brew install ${brew_deps[@]}"

execute_command_checked "wget https://www.python.org/ftp/python/3.8.18/Python-${pyversion}.tgz"
execute_command_checked "tar -xzf Python-${pyversion}.tgz"
execute_command_checked "cd Python-${pyversion}"
execute_command_checked "./configure --prefix=${HOME}/opt/python${pyversion} --enable-optimizations"
execute_command_checked "make -j"
execute_command_checked "make install"

pythonexe=${HOME}/opt/python${pyversion}/bin/python${vermajor}

execute_command_checked "${pythonexe} --version"

execute_command_checked "${pythonexe} -m pip install --upgrade pip"

execute_command_checked "${pythonexe} -m pip install virtualenv"

execute_command_checked "${pythonexe} -m venv ${HOME}/.pyenv/python${pyversion}"

printf "\n"
message_std "${yellow}================================================================================${normal}"
message_std "${yellow}=${normal} "
message_std "${yellow}=${normal} Activate Virtual Environment for Python 3.8"
message_std "${yellow}=${normal}  $ ${green}source \${HOME}/.pyenv/python${pyversion}/bin/activate${normal}"
message_std "${yellow}=${normal}  -or-"
message_std "${yellow}=${normal}  $ ${green}module load python/${pyversion}v"
message_std "${yellow}=${normal} "
message_std "${yellow}================================================================================${normal}"
printf "\n"

