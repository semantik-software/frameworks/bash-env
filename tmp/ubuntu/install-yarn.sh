#!/usr/bin/env bash
#
# Install common packages for my operating environment
#

# Package list for apt-get
ubuntu_deps=(
    npm
)


# Function that determines if the system is a ubuntu system
function is_ubuntu() {
    if [ -f /etc/os-release ]; then
        source /etc/os-release
        if [ "$ID" == "ubuntu" ]; then
            return 0
        fi
    fi
    return 1
}
export -f is_ubuntu

# Function that sets up colors for the terminal using tput if
# available, otherwise set color code envvars to empty strings.
# Color codes are: 'red','green','blue','magenta','cyan','yellow'
# Reset code is 'nocolor'
function setup_colors() {
    if command -v tput >/dev/null; then
        red=$(tput setaf 1)
        green=$(tput setaf 2)
        yellow=$(tput setaf 3)
        blue=$(tput setaf 4)
        magenta=$(tput setaf 5)
        cyan=$(tput setaf 6)
        gray=$(tput setaf 7)
        darkgray=$(tput setaf 8)
        orange=$(tput setaf 9)
        purple=$(tput setaf 56)
        nocolor=$(tput sgr0)
    else
        red=""
        green=""
        yellow=""
        blue=""
        magenta=""
        cyan=""
        gray=""
        darkgray=""
        orange=""
        purple=""
        nocolor=""
    fi
}

# Function that prints a message to the terminal
function message() {
    local text=${1:-' '}
    echo -e "${cyan}>>> ${yellow}${text}${nocolor}"
}

# Function that executes a command that is provided and
# checks the exit code. If the status code is nonzero,
# it prints a message indicating the failure. If it is
# successful then we print a success message.
function execute() {
    local cmd=${1:?}
    local success_msg=${2:-'PASS'}
    local failure_msg=${3:-'FAIL'}

    message "${blue}\$ ${magenta}${cmd}"
    eval "$cmd"
    local status=$?
    if [[ "${status}" == "0" ]]; then
        message "${green}${success_msg}"
    else
        message "${red}${failure_msg}"
        message "${red}Status code: ${status}"
    fi
}

# Function that executes a command that is provided and
# checks the exit code. If the status code is nonzero,
# it prints a message indicating the failure. If it is
# successful then we print a success message.
function execute_checked() {
    local cmd=${1:?}
    local success_msg=${2:-'PASS'}
    local failure_msg=${3:-'FAIL'}

    message "${blue}\$ ${magenta}${cmd}"
    eval "$cmd"
    local status=$?
    if [[ "${status}" == "0" ]]; then
        message "${green}${success_msg}"
    else
        message "${red}${failure_msg}"
        message "${red}Status code: ${status}"
        message "${red}Exiting..."
        exit ${status}
    fi
}

# ------------------------------
# Main
# ------------------------------
setup_colors

# Check if the system is ubuntu
message "Checking if the system is Ubuntu..."
if ! is_ubuntu; then
    message "${red}System is NOT Ubuntu"
    exit 1
else
    message "${green}System is Ubuntu"
fi

# Update and install dependencies
execute_checked "sudo apt-get update"
execute_checked "sudo apt-get upgrade -y"
execute_checked "sudo apt-get install -y ${ubuntu_deps[*]}"


# yarn
execute_command_checked "npm --version"
execute_command_checked "sudo npm install -g yarn"
# execute_command_checked "curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -"
# execute_command_checked "deb https://dl.yarnpkg.com/debian/ stable main | sudo tee /etc/apt/sources.list.d/yarn.list"
# execute_command_checked "sudo apt update"
# execute_command_checked "sudo apt install -y yarn"
execute_command_checked "yarn --version"

# execute_command_checked "sudo apt-add-repository -y "

