#!/usr/bin/env bash
#
# Downloads, Builds, and Installs python version into $HOME/opt/pythonX.Y.Z
#
# This does not use the deadsnakes archive.
#
vermajor=3
verminor=8
verpatch=19
pyversion="${vermajor}.${verminor}.${verpatch}"

rm Python-${pyversion}.tgz

ubuntu_deps=(
    make
    build-essential
    libssl-dev
    zlib1g-dev
    libbz2-dev
    libreadline-dev
    libsqlite3-dev
    wget
    curl
    llvm
    libncursesw5-dev
    xz-utils
    tk-dev
    libxml2-dev
    libxmlsec1-dev
    libffi-dev
    liblzma-dev
)
execute_command "sudo apt-get install -qy ${ubuntu_deps[*]}"

execute_command_checked "wget https://www.python.org/ftp/python/${pyversion:?}/Python-${pyversion:?}.tgz"

execute_command_checked "tar -xzf Python-${pyversion:?}.tgz"

execute_command_checked "cd Python-${pyversion:?}"

execute_command_checked "./configure --prefix=${HOME}/opt/python${pyversion:?} --enable-optimizations"
execute_command_checked "make -j"
execute_command_checked "make install"

pythonexe=${HOME}/opt/python${pyversion}/bin/python${vermajor}

execute_command_checked "${pythonexe} --version"

execute_command_checked "${pythonexe} -m pip install --upgrade pip"

execute_command_checked "${pythonexe} -m pip install virtualenv"

execute_command_checked "${pythonexe} -m venv ${HOME}/.pyenv/python${pyversion:?}"

printf "\n"
message_std "${yellow}================================================================================${normal}"
message_std "${yellow}=${normal} "
message_std "${yellow}=${normal} Activate Virtual Environment for Python ${pyversion:?}"
message_std "${yellow}=${normal}  $ ${green}source \${HOME}/.pyenv/python${pyversion:?}/bin/activate${normal}"
message_std "${yellow}=${normal}  -or-"
message_std "${yellow}=${normal}  $ ${green}module load python/${pyversion:?}"
message_std "${yellow}=${normal} "
message_std "${yellow}================================================================================${normal}"
printf "\n"

