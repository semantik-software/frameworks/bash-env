#!/usr/bin/env bash
#
# GitHub: https://github.com/p-ranav/argparse
#
export prefixpath=${HOME}/opt/cpplib

configflags=(
    -DCMAKE_INSTALL_PREFIX=${prefixpath:?}
    -DCMAKE_BUILD_TYPE=Release
)

# Set up prerequisites for building code
ubuntu_deps=(
    make
    build-essential
    rpm
    qt6-base-dev
)
execute_command "sudo apt-get install -qy ${ubuntu_deps[*]}"



# Download and extract the source code

packagename="argparse-3.0"
execute_command_checked "wget -O ${packagename:?}.tar.gz https://github.com/p-ranav/argparse/archive/refs/tags/v3.0.tar.gz"
# execute_command_checked "wget -O ${packagename:?}.tar.gz https://github.com/p-ranav/argparse/archive/refs/tags/v3.1.tar.gz"
execute_command_checked "tar -xzf ${packagename:?}.tar.gz"
execute_command_checked "pushd ${packagename:?}"
execute_command "rm -rf build"
execute_command_checked "mkdir build"
execute_command_checked "pushd build"

execute_command_checked "cmake ${configflags[*]} ../."
execute_command_checked "make -j10"
# execute_command "make test -j10"
execute_command_checked "make install -j4"

# Cleanup
execute_command "popd"
execute_command "popd"
execute_command "rm ${packagename:?}.tar.gz"
execute_command "rm -rf ${packagename:?}"

printf "\n"
message_std "${yellow}================================================================================${normal}"
message_std "${yellow}=${normal} "
message_std "${yellow}=${normal} C++ Library(s) installed in ${prefixpath:?}${normal}"
message_std "${yellow}=${normal} "
message_std "${yellow}================================================================================${normal}"
printf "\n"

