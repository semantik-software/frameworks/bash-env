#!/usr/bin/env bash
#
# Downloads, Builds, and Installs python version into $HOME/opt/pythonX.Y.Z
#
# This does not use the deadsnakes archive.
#
vermajor=3
verminor=30
verpatch=5
version="${vermajor}.${verminor}.${verpatch}"

rm cmake-${version}.tar.gz

# Set up prerequisites for building code
ubuntu_deps=(
    make
    build-essential
    rpm
    qt6-base-dev
)
execute_command "sudo apt-get install -qy ${ubuntu_deps[*]}"

# Download and extract the source code
execute_command_checked "wget https://github.com/Kitware/CMake/releases/download/v${version:?}/cmake-${version:?}.tar.gz"


execute_command_checked "tar -xzf cmake-${version:?}.tar.gz"

execute_command_checked "pushd cmake-${version:?}"

execute_command "rm -rf build"
execute_command_checked "mkdir build"
execute_command_checked "pushd build"

prefixpath=${HOME}/opt/cmake-${version:?}

configflags=(
  -DCMAKE_INSTALL_PREFIX=${prefixpath:?}
  -DCMAKE_BUILD_TYPE=Release
  -DBUILD_QtDialog=ON
)

# export CPPFLAGS="-O2"
execute_command_checked "cmake ${configflags[*]} ../."
execute_command_checked "make -j10"
execute_command "make test -j10"
execute_command_checked "make install -j4"

# Cleanup
execute_command "popd"
execute_command "popd"
execute_command "rm cmake-${version:?}.tar.gz"
execute_command "rm -rf cmake-${version:?}"

printf "\n"
message_std "${yellow}================================================================================${normal}"
message_std "${yellow}=${normal} "
message_std "${yellow}=${normal} CMake ${version:?} installed in ${prefixpath:?}${normal}"
message_std "${yellow}=${normal} "
message_std "${yellow}================================================================================${normal}"
printf "\n"

