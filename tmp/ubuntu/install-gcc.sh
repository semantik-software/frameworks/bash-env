#!/usr/bin/env bash
#
# Downloads, Builds, and Installs gcc
#
# This does not use the deadsnakes archive.
#
scriptdir=$(dirname $BASH_SOURCE)
scriptname=$(basename $BASH_SOURCE)
scriptdirabs=$(readlink -e ${scriptdir})

source ${scriptdir}/common.bash

versions=(
    '14.2.0'
    '14.1.0'
    '13.3.0'
    '12.4.0'
    '11.5.0'
)

# ==============================================================================
# Install Dependencies
# ==============================================================================
ubuntu_deps=(
    autoconf
    automake
    bison
    build-essential
    cmake
    curl
    gawk
    gcc-multilib
    g++-multilib
    gettext
    flex
    libbz2-dev
    libffi-dev
    liblzma-dev
    libgmp-dev
    libisl-dev
    libmpc-dev
    libmpfr-dev
    libncursesw5-dev
    libreadline-dev
    libsqlite3-dev
    libssl-dev
    libtool
    libtool-bin
    libxml2-dev
    libxmlsec1-dev
    llvm
    make
    ninja-build
    pkg-config
    texinfo
    tk-dev
    wget
    xz-utils
    zlib1g-dev
)
execute_command_checked "sudo apt-get update"
execute_command_checked "sudo apt-get upgrade -y"
execute_command_checked "sudo apt-get install -qy ${ubuntu_deps[*]}"


# ==============================================================================
# Install GCC
# ==============================================================================

function select_from_list() {
    local list=("$@")
    local selection
    local i=0
    for item in "${list[@]}"; do
        printf "%d: %s\n" $i "$item"
        i=$((i+1))
    done
    read -p "Select an option [0]: " selection
    selection=${selection:-0}
    echo "${list[$selection]}"
}

# Select the version of gcc to install
# version=$(select_from_list "${versions[@]}")
message_std "${cyan}Select GCC version to install:${normal}"
version=$(select_with_default "${versions[@]}")
message_std "Selected version: ${version}"

# Download the source code
if [ ! -f "gcc-${version:?}.tar.gz" ]; then
    execute_command_checked "wget https://ftp.gnu.org/gnu/gcc/gcc-${version:?}/gcc-${version:?}.tar.gz"
fi
if [ ! -f "gcc-${version:?}.tar.gz" ]; then
    message_err "Failed to download gcc-${version:?}.tar.gz"
    exit 1
fi
if [ ! -d "gcc-${version:?}" ]; then
    execute_command_checked "tar -xzf gcc-${version:?}.tar.gz"
fi
execute_command_checked "pushd gcc-${version:?}"

# Configure and build
execute_command_checked "./contrib/download_prerequisites"
if [ ! -d "build" ]; then
    execute_command_checked "mkdir build"
fi
execute_command_checked "pushd build"

options=(
    "--prefix=${HOME}/opt/gcc/${version:?}"
    "--disable-multilib"
    "--disable-fixincludes"
    "--enable-threads"
    "--with-system-zlib"
    # "--disable-bootstrap"
    # "--disable-libstdcxx-pch"
    "--enable-languages=c,c++,fortran,go"
    # "CXXFLAGS='-std=c++20'"
)
execute_command_checked "../configure ${options[*]}"

execute_command_checked "make -j"
execute_command_checked "make install"
execute_command_checked "popd"
execute_command_checked "popd"




# execute_command_checked "wget https://www.python.org/ftp/python/${pyversion:?}/Python-${pyversion:?}.tgz"

# execute_command_checked "tar -xzf Python-${pyversion:?}.tgz"

# execute_command_checked "cd Python-${pyversion:?}"

# execute_command_checked "./configure --prefix=${HOME}/opt/python${pyversion:?} --enable-optimizations"
# execute_command_checked "make -j"
# execute_command_checked "make install"

# pythonexe=${HOME}/opt/python${pyversion}/bin/python${vermajor}

# execute_command_checked "${pythonexe} --version"

# execute_command_checked "${pythonexe} -m pip install --upgrade pip"

# execute_command_checked "${pythonexe} -m pip install virtualenv"

# execute_command_checked "${pythonexe} -m venv ${HOME}/.pyenv/python${pyversion:?}"

# printf "\n"
# message_std "${yellow}================================================================================${normal}"
# message_std "${yellow}=${normal} "
# message_std "${yellow}=${normal} Activate Virtual Environment for Python ${pyversion:?}"
# message_std "${yellow}=${normal}  $ ${green}source \${HOME}/.pyenv/python${pyversion:?}/bin/activate${normal}"
# message_std "${yellow}=${normal}  -or-"
# message_std "${yellow}=${normal}  $ ${green}module load python/${pyversion:?}"
# message_std "${yellow}=${normal} "
# message_std "${yellow}================================================================================${normal}"
# printf "\n"

