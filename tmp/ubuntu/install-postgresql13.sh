#!/usr/bin/env bash
#
# Downloads, Builds, and Installs python version into $HOME/opt/pythonX.Y.Z
#
# This does not use the deadsnakes archive.
#
vermajor=13
verminor=16
version="${vermajor}.${verminor}"

rm postgresql-${version}.tar.gz

ubuntu_deps=(
    make
    build-essential
)

execute_command "sudo apt-get install -qy ${ubuntu_deps[*]}"

execute_command_checked "wget https://ftp.postgresql.org/pub/source/v${version:?}/postgresql-${version:?}.tar.gz"

execute_command_checked "tar -xzf postgresql-${version:?}.tar.gz"

execute_command_checked "cd postgresql-${version:?}"

prefixpath=${HOME}/opt/postgresql${version:?}

configflags=(
  --prefix=${prefixpath:?}
)

export CPPFLAGS="-O2"
execute_command_checked "./configure ${configflags[*]}"

execute_command_checked "make -j"

execute_command_checked "make install"


# pythonexe=${HOME}/opt/python${version}/bin/python${vermajor}

# execute_command_checked "${pythonexe} --version"

# execute_command_checked "${pythonexe} -m pip install --upgrade pip"

# execute_command_checked "${pythonexe} -m pip install virtualenv"

# execute_command_checked "${pythonexe} -m venv ${HOME}/.pyenv/python${version:?}"

printf "\n"
message_std "${yellow}================================================================================${normal}"
message_std "${yellow}=${normal} "
message_std "${yellow}=${normal} PostgreSQL ${version:?} installed in ${prefixpath:?}${normal}"
# message_std "${yellow}=${normal} Activate Virtual Environment for Python ${version:?}"
# message_std "${yellow}=${normal}  $ ${green}source \${HOME}/.pyenv/python${version:?}/bin/activate${normal}"
# message_std "${yellow}=${normal}  -or-"
# message_std "${yellow}=${normal}  $ ${green}module load python/${version:?}"
message_std "${yellow}=${normal} "
message_std "${yellow}================================================================================${normal}"
printf "\n"

