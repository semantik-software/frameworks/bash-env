#!/usr/bin/env bash
scriptdir=$(dirname $BASH_SOURCE)
scriptname=$(basename $BASH_SOURCE)
scriptdirabs=$(readlink -e ${scriptdir})

echo -e "${green}Load ${magenta}${scriptdirabs}/common.bash${normal}"

# executable_exists
#
# param1: executable (with path if necessary)
function executable_exists()
{
    local cmd=${1:?}
    local output=1
    if [ ! command -v ${cmd:?} &> /dev/null ]; then
        output=0
    fi
    echo ${output:?}
}
export -f executable_exists


# execute_command
#
# param1: command to execute
function execute_command()
{
    local command=${1:?}
    echo -e "# "
    echo -e "# E X E C U T E   C O M M A N D"
    echo -e "# "
    echo -e "$ ${magenta}${command:?}${normal}"

    [ ! -z ${DRYRUN} ] && return 0

    local is_executable=$(executable_exists ${command:?})

    if [[ "${is_executable}" == "1" ]]; then

        local _start=`date +%s`
        eval ${command:?}
        local err=$?
        local _stop=`date +%s`
        local _runtime=$((_stop-_start))

        if [ $err -ne 0 ]; then
            echo -e "${red}FAILED (${_runtime} s)${normal}"
        else
            echo -e "${green}OK (${_runtime} s)${normal}"
        fi
    else
        echo -e "${red}ERROR: command '${command:?}' is not executable"
        echo -e "${red}FAILED${normal}"
    fi
    return $err
}
export -f execute_command


# execute_command_checked
#
# param1: command to execute
function execute_command_checked()
{
    local command=${1:?}
    echo -e "# "
    echo -e "# E X E C U T E   C O M M A N D   C H E C K E D"
    echo -e "# "
    echo -e "$ ${magenta}${command:?}${normal}"

    [ ! -z ${DRYRUN} ] && return 0

    local is_executable=$(executable_exists ${command:?})

    if [[ "${is_executable}" == "1" ]]; then
        eval ${command:?}
        local err=$?
        if [ $err -ne 0 ]; then
            message_failure
            exit $err
        else
            echo -e "${green}OK${normal}"
        fi
    else
        print_message "${red}ERROR: command '${command:?}' is not executable"
        print_message "${red}FAILED${normal}"
        exit 32
    fi
}
export -f execute_command_checked


# select_with_default()
#
#   select_with_default [OPTIONS]
#
# Custom `select` implementation that allows *empty* input.
# Pass the choices as individual arguments.
# The *first* item in the options list is used as the default.
#
# Output is the chosen item, or "", if the user just pressed ENTER.
#
# Example:
#    my_choice=$(select_with_default 'one' 'two' 'three')
#  -or-
#    options_list=( "A" "B" "C" )
#    my_choice=$(select_with_default "${options_list[@]}}")
#
# If SELECT_OVERRIDE is set then we use that value, otherwise we
# query the user.
select_with_default()
{
    local item i=0 numItems=$#

    if [ ! -z ${SELECT_OVERRIDE} ]; then
        printf %s "${SELECT_OVERRIDE}"
        return
    fi

    if [[ "${numItems}" == "1" ]]; then
        printf %s "${1}"
        return
    fi

    local default_value=${1:?}

    # Print numbered menu items, based on the arguments passed.
    for item; do
        # Short for: for item in "$@"; do
        printf '%s' "$((++i))) $item"
        #if [[ "${i}" -eq "1" ]]; then
        #    printf '  (Default)'
        #fi
        printf '\n'
    done >&2 # Print to stderr, as `select` does.

    # Prompt the user for the index of the desired item.
    while :; do
        # Print the prompt string to stderr, as `select` does.
        printf %s "${PS3-Select Option Number [${magenta}1: ${default_value}${normal}]> }" >&2

        read -r index
        # Make sure that the input is either empty or that a valid index was entered.
        [[ -z $index ]] && break  # empty input
        (( index >= 1 && index <= numItems )) 2>/dev/null || { echo "Invalid selection. Please try again." >&2; continue; }
        break
    done

    if [ -z $index ]; then
        index=1
    fi

    # debugging - prints to stderr
    #echo -e ">>> index '${index}'" >&2
    #echo -e ">>> opt1 '${1}'" >&2
    #echo -e ">>> opt2 '${1}'" >&2

    # Output the selected item, if any.
    [[ -n $index ]] && printf %s "${@: index:1}"
}
export -f select_with_default


