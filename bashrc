#!/usr/bin/env bash
#
# Top-Level bashrc script
# =======================
# Source this file from your $HOME/.bashrc or $HOME/.bash_profile scripts
#
scriptpath=$(dirname  $BASH_SOURCE)
scriptname=$(basename $BASH_SOURCE)

# Don't do anything if not running interactively
[ -z "$PS1" ] && return


# Get the directory that this bashrc script lives in
bashrc_get_root() {
    local DIR=""
    local SOURCE="${BASH_SOURCE[0]}"
    while [ -h "${SOURCE}" ]; do
        DIR="$( cd -P "$( dirname "${SOURCE}" )" >/dev/null 2>&1 && pwd )"
        SOURCE="$(readlink "${SOURCE}")"
        [[ ${SOURCE} != /* ]] && SOURCE="${DIR}/${SOURCE}"
    done
    local DIR="$( cd -P "$( dirname "${SOURCE}" )" >/dev/null 2>&1 && pwd )"

    echo "${DIR:?}"
}

ulimit -S -c 0
set -o notify
set -o noclobber
set -o ignoreeof


export BASHRC_ROOT=$(bashrc_get_root)
#echo "BASHRC_ROOT=${BASHRC_ROOT:?}"

if [ -f ${BASHRC_ROOT:?}/etc/bashrc ]; then
    source ${BASHRC_ROOT:?}/etc/bashrc
fi

# Uncomment to load user-specific bashrc.d files in your
# $HOME/etc/bashrc.d directory.
# You can change the path if you want your user or system-specific
# settings
#bashrc-load ${HOME:?}/etc/bashrc.d


