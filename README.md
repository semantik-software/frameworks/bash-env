bash-env
========
This is my **bash** environment setup.

The general idea behind this is to be able to break a shell initialization away from
a monolithic `.bashrc` or `.bash_profile` file and into a group of ordered `*.bashrc` files
in a "dot d" folder similar to `/etc/init.d` or `/etc/profile.d` style folders in which
the files dropped into the folder get loaded.

This framework can be cloned anywhere on your system and then enabled by adding
the following line to your `${HOME}/.bashrc` file:

```bash
export BASHENV_ROOT=<prefix path to the cloned copy of bash-env>
[ -f ${BASHENV_ROOT}/etc/bashrc ] && source ${BASHENV_ROOT}/etc/bashrc
```

In this implementation, `${BASHENV_ROOT}/etc/bashrc` is a script that will load all
the bashrc scripts that are in `${BASHENV_ROOT}/etc/bashrc.d/` and match the file
name pattern `????_*.bashrc`.

Files are loaded in ascending sorted order. So I might load these files:

| Filename                                | Active? | Description                                                                          |
| --------------------------------------- | ------- | ------------------------------------------------------------------------------------ |
| `0000_colors.bashrc`                    | YES     | Sets up color codes for the environment like `${red}`, `${green}`, `${normal}`, etc. |
| `0001_functions_bootstrap.bashrc`       | YES     | Bootstrapping functions - sets up some basic framework functions for bash-env.       |
| `0002_functions_messages.bashrc`        | YES     | Sets up functions for writing messages to the console in some standard way.          |
| `0005_functions_common.bashrc`          | YES     | Sets up common functions that are useful.                                            |
| `0005_functions_exec.bashrc`            | YES     | Sets up common functions related to executing things.                                |
| `0005_functions_git.bashrc`             | YES     | Git related functions and helpers                                                    |
| `0005_lscolors_dark.bashrc`             | YES     | Custom `ls` colors for directory listings (DARK).                                    |
| `0005_lscolors_light.bashrc.x`          | NO      | Custom `ls` colors for directory listings (LIGHT).                                   |
| `0010_alias.bashrc`                     | YES     | Sets up some basic/universal aliases.                                                |
| `0010_bash_completion.bashrc`           | YES     | Sets up bash completion helpers                                                      |
| `0010_envconfig.bashrc`                 | YES     | Set up common environment settings like `HISTSIZE`, `ulimit`, etc.                   |
| `0010_lmod.bashrc`                      | YES     | LMOD related settings                                                                |
| `0099_prompt_type_a.bashrc.x`           | NO      | Prompt variant 'a'                                                                   |
| `0099_prompt_type_b.bashrc`             | YES     | Prompt vairant 'b'                                                                   |
| `0100_osx_application_shortcuts.bashrc` | YES     | OSX (Darwin) shortcuts                                                               |
| `0100_osx_homebrew_config.bashrc`       | YES     | OSX (Darwin) homebrew configurations.                                                |
| `XXXX_template.bashrc.x`                | NO      | Template file for a new login script.                                                |

Some Additional helper scripts:

| Filename       | Description                                                                                        |
| -------------- | -------------------------------------------------------------------------------------------------- |
| git-prompt.sh  | A script I downloaded that sets up `git` properties for prompt setup.                              |
| loadenv.bashrc | A helper script that will load all of the `????_*.bashrc` files directly. (not used by framework). |



