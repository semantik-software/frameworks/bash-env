#!/usr/bin/env bash
scriptpath=$(dirname  ${BASH_SOURCE:?})
scriptname=$(basename ${BASH_SOURCE:?})
loadenv_notify ${scriptname}


# get the top-level directory of the current
# repository that the current-working-directory
# is contained in.
bashrc_git_root() {
    local git_root=$(git rev-parse --show-toplevel)
    echo "${git_root:?}"
}
export -f bashrc_git_root


bashrc_git_bashrc_dir() {
    local git_root=$(bashrc_git_root)
    echo "${git_root}/etc/bashrc.d"
}
export -f bashrc_git_bashrc_dir


git_tags_list() {
    for tag in $(git tag --list); do
        sha=$(git rev-list -1 $tag)
        echo "$tag: $sha"
    done
}
export -f git_tags_list


