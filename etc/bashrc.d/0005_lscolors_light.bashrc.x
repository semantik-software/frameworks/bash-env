#!/usr/bin/env bash
scriptdir=$(dirname $BASH_SOURCE)
scriptname=$(basename $BASH_SOURCE)
loadenv_notify ${scriptname}

#------------------------------------------------------
# LS_COLORS configuration for directory listings
#------------------------------------------------------
#
# http://linux-sxs.org/housekeeping/lscolors.html
#
# http://askubuntu.com/questions/466198/how-do-i-change-the-color-for-directories-with-ls-in-the-console
#
unset LS_COLORS
unset CLICOLOR
unset LSCOLORS

#if [[ "$PLATFORM" == 'darwin' ]]; then

# directory listing coloring for default LS in darwin
export CLICOLOR=1
export LSCOLORS="GxFxCxDxBxegedabagaced"

#else

# Coloring for Linux or OSX (with coreutils installed)
# 0;30 = black
# 0;31 = red
# 0;32 = green
# 0;33 = yellow
# 0;34 = blue
# 0;35 = purple
# 0;36 = cyan
# 1;37 = white
# 0;38 = normal?
# 0;90 = dark gray
# 0;92 = darker gray
# 0;95 = dim white
exec="0;31"          # Red
dirs="0;36"          # Cyan
zips="0;35"          # Purple
sources="0;33"       # Yellow
#sourceobj="$(color 12)"
sourceobj="0;35"
#dimfile="0;38"
dimfile="0;90"
text_files="0;35"    # White
image_files="0;32"   # Green
bashrc="0;32"        # White

LS_COLORS="di=${dirs}:fi=0;0:ln=0;35:pi=5:so=5:bd=5:cd=5:or=5;33;40:mi=0;0:ex=${exec}"
LS_COLORS="${LS_COLORS}:*.zip=${zips}:*.tgz=${zips}:*.gz=${zips}:*.tar.gz=${zips}:*.bz2=${zips}"
LS_COLORS="${LS_COLORS}:*.py=${sources}:*.cpp=${sources}:*.hpp=${sources}:*.h=${sources}:*.groovy=${sources}"
LS_COLORS="${LS_COLORS}:*.jenkinsfile=${source}"
LS_COLORS="${LS_COLORS}:*.pyc=${sourceobj}:*.pyo=${sourceobj}:*.o=${sourceobj}"
LS_COLORS="${LS_COLORS}:*.md=${text_files}:*.txt=${text_files}"
LS_COLORS="${LS_COLORS}:*.bashrc=${bashrc}:*.sh=${bashrc}:*.bash=${bashrc}"
LS_COLORS="${LS_COLORS}:*.bash.x=${dimfile}:*.bashrc.x=${dimfile}"
LS_COLORS="${LS_COLORS}:*.jpg=${image_files}:*.png=${image_files}:*.xcf=${image_files}"
export LS_COLORS

#fi
