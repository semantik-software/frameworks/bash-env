#!/usr/bin/env bash
scriptpath=$(dirname  ${BASH_SOURCE:?})
scriptname=$(basename ${BASH_SOURCE:?})
loadenv_notify ${scriptname}


# -----------------------------------
#   C O M M O N   U T I L I T I E S
# -----------------------------------


# get the top-level directory of the git repository
bashrc_git_root() {
    #local scriptpath=$(dirname  ${BASH_SOURCE:?})
    #local scriptname=$(basename ${BASH_SOURCE:?})
    local git_root=$(git rev-parse --show-toplevel)
    echo "${git_root:?}"
}
export -f bashrc_git_root


bashrc_git_bashrc_dir() {
    local git_root=$(bashrc_git_root)
    echo "${git_root}/etc/bashrc.d"
}
export -f bashrc_git_bashrc_dir


# bashrc_scriptpath_abs
# - Get the absolute path to the current script, regardless
#   of CWD. This should also work when SOURCING the script.
function bashrc_scriptpath_abs() {
    #echo "bashrc_scriptpath_abs()" >&2
    local scriptname_abs="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/$(basename "${BASH_SOURCE[0]}")"
    local scriptpath_abs="$(dirname "${scriptname_abs:?}")"
    echo "${scriptpath_abs:?}"
}
export -f bashrc_scriptpath_abs



#============================================================
#
#  ALIASES AND FUNCTIONS
#
#  Arguably, some functions defined here are quite big.
#  If you want to make this file smaller, these functions can
#+ be converted into scripts and removed from here.
#
#============================================================

#-------------------------------------------------------------
# File & strings related functions:
#-------------------------------------------------------------

# Find a file with a pattern in name:
function rgrep() { grep -R -S -n --color=always "$1" * ; }
export -f rgrep

# Find a file with a pattern in name:
function ff() { find . -type f -iname '*'"$*"'*' -ls ; }
export -f ff

# Find a file with pattern $1 in name and Execute $2 on it:
function fe() { find . -type f -iname '*'"${1:-}"'*' \
-exec ${2:-file} {} \;  ; }
export -f fe

#  Find a pattern in a set of files and highlight them:
#+ (needs a recent version of egrep).
function fstr()
{
    OPTIND=1
    local mycase=""
    local usage="fstr: find string in files.
Usage: fstr [-i] \"pattern\" [\"filename pattern\"] "
    while getopts :it opt
    do
        case "$opt" in
           i) mycase="-i " ;;
           *) echo "$usage"; return ;;
        esac
    done
    shift $(( $OPTIND - 1 ))
    if [ "$#" -lt 1 ]; then
        echo "$usage"
        return;
    fi
    find . -type f -name "${2:-*}" -print0 | \
xargs -0 egrep --color=always -sn ${case} "$1" 2>&- | more

}
export -f fstr


function swap()
{ # Swap 2 filenames around, if they exist (from Uzi's bashrc).
    local TMPFILE=tmp.$$

    [ $# -ne 2 ] && echo "swap: 2 arguments needed" && return 1
    [ ! -e $1 ] && echo "swap: $1 does not exist" && return 1
    [ ! -e $2 ] && echo "swap: $2 does not exist" && return 1

    mv "$1" $TMPFILE
    mv "$2" "$1"
    mv $TMPFILE "$2"
}
export -f swap


function extract()      # Handy Extract Program
{
    if [ -f $1 ] ; then
        case $1 in
            *.tar.bz2)   tar xvjf $1     ;;
            *.tar.gz)    tar xvzf $1     ;;
            *.bz2)       bunzip2 $1      ;;
            *.rar)       unrar x $1      ;;
            *.gz)        gunzip $1       ;;
            *.tar)       tar xvf $1      ;;
            *.tbz2)      tar xvjf $1     ;;
            *.tgz)       tar xvzf $1     ;;
            *.zip)       unzip $1        ;;
            *.Z)         uncompress $1   ;;
            *.7z)        7z x $1         ;;
            *)           echo "'$1' cannot be extracted via >extract<" ;;
        esac
    else
        echo "'$1' is not a valid file!"
    fi
}
export -f extract


# Creates an archive (*.tar.gz) from given directory.
function maketar() { tar cvzf "${1%%/}.tar.gz"  "${1%%/}/"; }
export -f maketar

# Create a ZIP archive of a file or folder.
function makezip() { zip -T -r "${1%%/}.zip" "$1" ; }
export -f makezip

# Make your directories and files access rights sane.
function sanitize() { chmod -R u=rwX,g=rX,o= "$@" ;}
export -f sanitize

#-------------------------------------------------------------
# Misc utilities:
#-------------------------------------------------------------

function repeat()       # Repeat n times command.
{
    local i max
    max=$1; shift;
    for ((i=1; i <= max ; i++)); do  # --> C-like syntax
        eval "$@";
    done
}
export -f repeat


#-------------------------------------------------------------
# Timeout Utility (for Darwin only)
#-------------------------------------------------------------
if [[ "$PLATFORM" == "darwin" ]]; then

function timeout()
{
  perl -e 'alarm shift; exec @ARGV' "$@";
}

fi


#-------------------------------------------------------------
# Process/system related functions:
#-------------------------------------------------------------

function my_ps() { ps $@ -u $USER -o pid,%cpu,%mem,bsdtime,command ; }
function pp() { my_ps f | awk '!/awk/ && $0~var' var=${1:-".*"} ; }

export -f my_ps
export -f pp

function mydf()         # Pretty-print of 'df' output.
{                       # Inspired by 'dfc' utility.
    for fs ; do

        if [ ! -d $fs ]
        then
          echo -e $fs" :No such file or directory" ; continue
        fi

        local info=( $(command df -P $fs | awk 'END{ print $2,$3,$5 }') )
        local free=( $(command df -Pkh $fs | awk 'END{ print $4 }') )
        local nbstars=$(( 20 * ${info[1]} / ${info[0]} ))
        local out="["
        for ((j=0;j<20;j++)); do
            if [ ${j} -lt ${nbstars} ]; then
               out=$out"*"
            else
               out=$out"-"
            fi
        done
        out=${info[2]}" "$out"] ("$free" free on "$fs")"
        echo -e $out
    done
}
export -f mydf


function my_ip() # Get IP adress on ethernet.
{
    MY_IP=$(/sbin/ifconfig eth0 | awk '/inet/ { print $2 } ' | sed -e s/addr://)
    echo ${MY_IP:-"Not connected"}
}
export -f my_ip


function ii()   # Get current host related info.
{
    echo -e "\nYou are logged on $HOST"
    echo -e "\nAdditionnal information: " ; uname -a
    echo -e "\nUsers logged on: " ; w -hi | cut -d " " -f1 | sort | uniq
    echo -e "\nCurrent date : " ; date
    echo -e "\nMachine stats : " ; uptime
    echo -e "\nMemory stats : " ; free
    echo -e "\nDiskspace : " ; mydf / $HOME
    echo -e "\nLocal IP Address :" ; my_ip
    #echo -e "\nOpen connections : "; netstat -an -p tcp;
    echo ""
}
export -f ii


function set_session_type()
{
    SESSION_TYPE=""
    if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
        SESSION_TYPE=remote/ssh
    # many other test cases should go here.
    else
        case $(ps -o comm= -p $PPID) in
            sshd|*/sshd) SESSION_TYPE=remote/ssh;;
        esac
    fi
    export SESSION_TYPE
}
export -f set_session_type


function banner_welcome() {
    if hash toilet 2>/dev/null; then
        # toilet -w90 -f smblock --filter border:metal Welcome to `hostname`
        # toilet -w90 -f smblock --filter border:gay Welcome to `hostname`
        toilet -w90 -f future --filter border Welcome to `hostname`
    elif hash figlet 2>/dev/null; then
        figlet -w 90 -f threepoint Welcome to `hostname`
    else
        echo "============================================"
        echo " Welcome to" `hostname`
        echo "============================================"
    fi
}
export -f banner_welcome


# checked_module_load()
#   Conditionally loads a module from an environment var with a check against the
#   value of said envvar to ensure it's valid.  Load a default module if the given
#   envvar fails the check.
#
#     Param1: (str) REGEXP to check SEMS module name.
#     Param2: (str) Name of the envvar containing a module to load.
#     Param3: (str) Default module to load.
function checked_module_load() {
    echo "checked_module_load()"
    if [[ ! -n "${!2+1}" ]]; then
        echo "... ${2} is not set"
        echo "... Loading module: '${3}'"
        module load ${3}
    elif [[ "${!2}" =~ ${1} ]]; then
        echo "... ${2} matched ${!2}"
        echo "... This is a valid SEMS module of the format '${1}'."
        echo "... Loading module: '${2}'"
        module load ${!2}
    else
        echo "... ${2} did not match '${!2}'"
        echo "... This is NOT a valid SEMS module of the format '${1}'"
        echo "... Loading module: '${3}'"
        module load ${3}
    fi
}
export -f checked_module_load


# envvar_append_or_create_sep
#  $1 = envvar separator
#  $2 = envvar name
#  $3 = string to append
function envvar_append_or_create_sep() {
    local envvar_sep=${1:?}
    local envvar_name=${2:?}
    local envvar_value=${3:?}
    #echo "envvar_sep = ${envvar_sep}" >&2
    #echo "envvar_sep = ${envvar_name}" >&2
    #echo "envvar_sep = ${envvar_value}" >&2

    if [[ ! -n "${!envvar_name+1}" ]]; then
        export ${envvar_name}="${envvar_value}"
    else
        #[ "${!envvar_name#*$envvar_value:}" == "${!envvar_name}" ] && export ${envvar_name}="${!envvar_name}${envvar_sep}${envvar_value}"
        [ "${!envvar_name#*$envvar_value}" == "${!envvar_name}" ] && export ${envvar_name}="${!envvar_name}${envvar_sep}${envvar_value}"
    fi
}
export -f envvar_append_or_create_sep


# envvar_prepend_or_create_sep
#  $1 = separator
#  $2 = envvar name
#  $3 = string to prepend
function envvar_prepend_or_create_sep() {
    local envvar_sep=${1:?}
    local envvar_name=${2:?}
    local envvar_value=${3:?}
    #echo "envvar_sep   = ${envvar_sep}" >&2
    #echo "envvar_name  = ${envvar_name}" >&2
    #echo "envvar_value = ${envvar_value}" >&2

    # envvar $1 is not set
    if [[ ! -n "${!envvar_name+1}" ]]; then
        export ${envvar_name}="${envvar_value}"
    else
        #[ "${!envvar_name#*$envvar_value:}" == "${!envvar_name}" ] && export ${envvar_name}="${envvar_value}${envvar_sep}${!envvar_name}"
        [ "${!envvar_name#*$envvar_value}" == "${!envvar_name}" ] && export ${envvar_name}="${envvar_value}${envvar_sep}${!envvar_name}"
        #export ${envvar_name}="${envvar_value}${envvar_sep}${!envvar_name}"
    fi
}
export -f envvar_prepend_or_create_sep


# envvar_append_or_create
#  $1 = envvar name
#  $2 = string to append
function envvar_append_or_create() {
    local var=${1:?}
    local val=${2:?}
    #echo "var = ${var}" >&2
    #echo "val = ${val}" >&2
    envvar_append_or_create_sep ":" ${var} ${val}
}
export -f envvar_append_or_create


# envvar_prepend_or_create
#  $1 = envvar name
#  $2 = string to prepend
function envvar_prepend_or_create() {
    # envvar $1 is not set
    local var=${1:?}
    local val=${2:?}
    #echo "var = ${var}" >&2
    #echo "val = ${val}" >&2
    envvar_prepend_or_create_sep ":" ${var} ${val}
}
export -f envvar_prepend_or_create


# envvar_set_or_create
#  $1 = envvar name
#  $2 = string to prepend
function envvar_set_or_create() {
    export ${1}="${2}"
}
export -f envvar_set_or_create


## envvar_remove_substr
## $1 = envvar name
## $2 = substring to remove
#function envvar_remove_substr() {
#    local envvar=${1:?}
#    local substr=${2:?}
#    if [ ! -z ${1:?} ]; then
#        export ${envvar}=$(echo ${!envvar} | sed s/${substr}//g)
#    fi
#}


# envvar_remove_substr
# $1 = envvar name
# $2 = substring to remove
function envvar_remove_substr() {
    local altsep="-::-"
    local envvar=${1:?}
    local substr=${2:?}
    if [ ! -z ${1:?} ]; then
        # swap any '#' characters with altsep for the replace.
        substr=$(echo ${substr} | sed s/\#/${altsep}/g)
        #echo "substr = ${substr}" > /dev/stdout
        #echo "envvar = ${!envvar}" > /dev/stdout
        export ${envvar}=$(echo ${!envvar} | sed s#\\##${altsep}#g)
        export ${envvar}=$(echo ${!envvar} | sed s#${substr}##g)
        #echo "envvar = ${!envvar}" > /dev/stdout
        export ${envvar}=$(echo ${!envvar} | sed s/${altsep}/\#/g)
        #echo "envvar = ${!envvar}" > /dev/stdout
    fi
}
export -f envvar_remove_substr


# envvar_remove_path_entry
# $1 = A path style envvar name
# $2 = Entry to remove from the path.
function envvar_remove_path_entry() {
    local envvar=${1:?}
    local to_remove=${2:?}
    local new_value=${!envvar}
    if [ ! -z ${envvar} ]; then
        new_value=:${new_value}:
        new_value=${new_value//:${to_remove}:/:}
        new_value=${new_value#:1}
        new_value=${new_value%:1}
        export ${envvar}=${new_value}
    fi
}
export -f envvar_remove_path_entry


# param 1: Title
# param 2: Text
function notification() {
    if [ -f /usr/bin/osascript ]; then
        /usr/bin/osascript \
          -e "set theDialogText to \"${2}\n\" & (current date)" \
          -e 'set homeDir to path to home folder as string' \
          -e 'set failIco to homeDir & "etc:icons:Fail.icns"' \
          -e 'set passIco to homeDir & "etc:icons:Pass.icns"' \
          -e "display dialog  theDialogText buttons {\"Ok\"} default button \"Ok\" with title \"$1\" with icon file passIco"
    else
        echo "=====[ $1 ]=================================="
        echo "="
        echo "= $2"
        echo "="
        echo "= ($(date))"
        echo "="
    fi
}
export -f notification


# param 1: Title
# param 2: Text
function notification_pass() {
    if [ -f /usr/bin/osascript ]; then
        /usr/bin/osascript \
          -e "set theDialogText to \"${2} PASSED\n\" & (current date)" \
          -e 'set homeDir to path to home folder as string' \
          -e 'set failIco to homeDir & "etc:icons:Fail.icns"' \
          -e 'set passIco to homeDir & "etc:icons:Pass.icns"' \
          -e 'set boratHappy to homeDir & "etc:icons:BoratHappy.jpg"' \
          -e 'set klingonSuccess to homeDir & "etc:icons:klingon-success.jpg"' \
          -e "display dialog theDialogText buttons {\"Ok\"} default button \"Ok\" with title \"$1\" with icon file boratHappy"
    else
        echo "=====[ $1 ]=================================="
        echo "="
        echo "= $2 PASSED"
        echo "="
        echo "= ($(date))"
        echo "="
    fi
}
export -f notification_pass


# param 1: Title
# param 2: Text
function notification_fail() {
    if [ -f /usr/bin/osascript ]; then
        /usr/bin/osascript \
          -e "set theDialogText to \"${2} FAILED\n\" & (current date)" \
          -e 'set homeDir to path to home folder as string' \
          -e 'set failIco to homeDir & "etc:icons:Fail.icns"' \
          -e 'set passIco to homeDir & "etc:icons:Pass.icns"' \
          -e 'set klingonFailure to homeDir & "etc:icons:klingon-failure.jpg"' \
          -e 'set klingonSuccess to homeDir & "etc:icons:klingon-success.jpg"' \
          -e "display dialog  theDialogText buttons {\"Ok\"} default button \"Ok\" with title \"$1\" with icon file klingonSuccess"
    else
        echo "=====[ $1 ]=================================="
        echo "="
        echo "= $2 FAILED"
        echo "="
        echo "= ($(date))"
        echo "="
    fi
}
export -f notification_fail


# param 1: Title
# param 2: Text
function notice() {
    if [ -f /usr/bin/osascript ]; then
        /usr/bin/osascript \
          -e 'set sysIconPath to "/System/Library/CoreServices/CoreTypes.bundle/Contents/Resources/"' \
          -e 'set theIcon to sysIconPath & "FinderIcon.icns"' \
          -e "set theTitle to \"${1}\"" \
          -e "set theText to \"${2}\""  \
          -e "display dialog theText buttons {\"Ok\"} with title theTitle with icon {theIcon}"
    else
        echo "=====[ $1 ]=================================="
        echo "="
        echo "= $2"
        echo "="
        echo "= ($(date))"
        echo "="
    fi
}
export -f notice


function random_string_32() {
    #local random_32=$(date "+%s-%N" | sha256sum | base64 | head -c 32)
    local random_32=$(echo "${RANDOM}" | sha256sum | base64 | head -c 32)
    echo -e "${random_32}"
}
export -f random_string_32


function random_password_20() {
    #local random_32=$(date "+%s-%N" | sha256sum | base64 | head -c 32)
    local group_01=$(echo "${RANDOM}" | sha256sum | base64 | head -c 4)
    local group_02=$(echo "${RANDOM}" | sha256sum | base64 | head -c 4)
    local group_03=$(echo "${RANDOM}" | sha256sum | base64 | head -c 4)
    local group_04=$(echo "${RANDOM}" | sha256sum | base64 | head -c 4)
    echo -e "${group_01}-${group_02}-${group_03}-${group_04}"
}
export -f random_password_20


function string_exists_in_file() {
    local filename=${1:?}
    local string=${2:?}
    local exists=`cat ${filename} | grep ${string} | wc -l`
    echo ${exists}
}
export -f string_exists_in_file


# Param 1: volume name
# Param 2: usage max %
function volcheck() {
    local volume_name=${1:?}
    local use_max=$(echo ${2:?} | cut -d'%' -f1)

    #echo -e "volume_name = ${volume_name:?}" > /dev/stdout

    local output=$(df -H ${volume_name:?} | sed -n '2 p')
    #echo -e "output = ${output}" > /dev/stdout

    local usep=$(echo $output | awk '{ match($0, /([0-9]+%)/, arr); if(arr[1] != "") print arr[1] }' | cut -d'%' -f1 )
    #echo "usep = ${usep}" > /dev/stdout

    if [ ${usep:?} -ge ${use_max:?} ]; then
        echo -e "${yellow}VOLCHECK${normal}: Volume ${magenta}${volume_name:?}${normal} usage is ${magenta}${usep:?}%${normal} ${red}WARNING${normal}" > /dev/stdout
        return 1
    fi
    echo -e "${yellow}VOLCHECK${normal}: Volume ${magenta}${volume_name:?}${normal} usage is ${magenta}${usep:?}%${normal} ${green}OK${normal}" > /dev/stdout
    #echo -e "VOLCHECK: Volume ${volume_name:?} usage is ${usep:?}% ${green}OK${normal}" > /dev/stdout
    return 0
}
export -f volcheck


# reset verbosity
set +x


# Gets the current script name (full path + filename)
function get_scriptname() {
    # Get the full path to the current script
    local script_name=`basename $0`
    local script_path=$(dirname $(readlink -f $0))
    local script_file="${script_path}/${script_name:?}"
    echo "${script_file}"
}
export -f get_scriptname


# Gets the path to the current script (full path)
function get_scriptpath() {
    # Get the full path to the current script
    local script_name=`basename $0`
    local script_path=$(dirname $(readlink -f $0))
    echo "${script_path}"
}
export -f get_scriptpath


# Get the md5sum of a filename.
# param1: filename
# returns: md5sum of the file.
function get_md5sum() {
    local filename=${1:?}
    local sig=$(md5sum ${filename:?} | cut -d' ' -f1)
    echo "${sig:?}"
}
export -f get_md5sum


# Generate a random string
# param1: string_length (1..256)
function random_string_alnum() {
    local len=${1:?}

    if [ $len -gt 256 ]; then
        len=256
    elif [ $len -lt 1 ]; then
        len=1
    fi

    local output=""
    for n in [ 1 2 3 4 5 6 7 8 ]; do
        output="${output}$(echo ${RANDOM} | sha256sum | base64 | cut -d ' ' -f 1 | head -c 32)"
    done
    output=$(echo ${output} | head -c ${len})
    echo -e "${output}"
}
export -f random_string_alnum


# select_with_default()
#
#   select_with_default [OPTIONS]
#
# Custom `select` implementation that allows *empty* input.
# Pass the choices as individual arguments.
# The *first* item in the options list is used as the default.
#
# Output is the chosen item, or "", if the user just pressed ENTER.
#
# Example:
#    my_choice=$(select_with_default 'one' 'two' 'three')
#  -or-
#    options_list=( "A" "B" "C" )
#    my_choice=$(select_with_default "${options_list[@]}}")
#
# Attribution: This is copied with minor modifications from here:
#     https://stackoverflow.com/a/42790579/2059999
#
# If SELECTION_OVERRIDE is set then we use that value, otherwise we
# query the user.
#
select_with_default() {

    local item i=0 numItems=$#

    if [ ! -z ${SELECTION_OVERRIDE} ]; then
        printf %s "${SELECTION_OVERRIDE}"
        return
    fi

    if [[ "${numItems}" == "1" ]]; then
        printf %s "${1}"
        return
    fi

    local default_value=${1:?}

    # Print numbered menu items, based on the arguments passed.
    for item; do
        # Short for: for item in "$@"; do
        printf '%s' "$((++i))) $item"
        #if [[ "${i}" -eq "1" ]]; then
        #    printf '  (Default)'
        #fi
        printf '\n'
    done >&2 # Print to stderr, as `select` does.

    # Prompt the user for the index of the desired item.
    while :; do
        # Print the prompt string to stderr, as `select` does.
        printf %s "${PS3-Select Option Number [1:${default_value}]> }" >&2

        read -r index
        # Make sure that the input is either empty or that a valid index was entered.
        [[ -z $index ]] && break  # empty input
        (( index >= 1 && index <= numItems )) 2>/dev/null || { echo "Invalid selection. Please try again." >&2; continue; }
        break
    done

    if [ -z $index ]; then
        index=1
    fi

    #echo -e ">>> index '${index}'" >&2
    #echo -e ">>> opt1 '${1}'" >&2
    #echo -e ">>> opt2 '${1}'" >&2

    # Output the selected item, if any.
    [[ -n $index ]] && printf %s "${@: index:1}"
}
export -f select_with_default


# param 1: executable
function is_installed() {
    local executable=${1:?}
    which -s ${executable:?}
    if [[ $? == 0 ]]; then
        echo "1"
    else
        echo "0"
    fi
}
export -f is_installed


# param 1: venv path
function activate_venv() {
    local venv_root=${1:?}
    if [[ -e ${venv_root:?}/bin/activate ]]; then
        echo -e "${cyan}Activate venv${normal}"
        source ${venv_root:?}/bin/activate
    fi
}
export -f activate_venv



