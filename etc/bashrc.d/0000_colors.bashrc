#!/usr/bin/env bash

if [[ ! -z $BASH_SOURCE ]]; then
    scriptpath=$(dirname  ${BASH_SOURCE:?})
    scriptname=$(basename ${BASH_SOURCE:?})
else
    ZSH_SOURCE=${(%):-%x}
    echo -e "ZSH_SOURCE=${ZSH_SOURCE}"
fi

# Get the current scheme (light or dark)
# See: https://unix.stackexchange.com/questions/701432/command-for-detecting-whether-the-system-is-using-a-dark-or-light-desktop-theme
# -- might not work in ubuntu 23.04
#function get_theme_type() {
#    scheme=$(
#      gdbus call --session --timeout=1000 \
#                 --dest=org.freedesktop.portal.Desktop \
#                 --object-path /org/freedesktop/portal/desktop \
#                 --method org.freedesktop.portal.Settings.Read org.freedesktop.appearance color-scheme
#    )
#
#    case $scheme in
#        ( '(<<uint32 1>>,)' ) echo "LIGHT"   ;;
#        ( '(<<uint32 2>>,)' ) echo "DARK"    ;;
#        ( *                 ) echo "UNKNOWN" ;;
#    esac
#}
#export -f get_theme_type

# Set a color by color number.
# Usage: echo -e "$(color 4)mytext"
# param1: the color number (int)
function color() {
    local _color=${1:?}
    local _numcolors=`tput colors`
    if [ -n ${_numcolors} ] && [ ${_numcolors} -gt ${_color} ]; then
        printf "$(tput setaf ${_color})"
    fi
}
export -f color



 #Print out the color table
function print_color_table() {
    local num_colors=`tput colors`
    echo "num_colors=${num_colors}" >&2
    for ((i=0; i<${num_colors}; i++)); do
        local color=$(tput setaf ${i})
        printf "${color}%4s${normal}" $i
        if [[ "$(( ($i+1) % 20))" == "0" ]]; then
            printf "\n"
        fi
    done
}
export -f print_color_table


# ========================
# Set up some named colors
# ========================

unset bold
unset underline
unset standout
unset standout_end
unset normal
unset dim
unset black
unset red
unset green
unset yellow
unset blue
unset magenta
unset cyan
unset ice
unset white
unset purple
unset gray
unset ltred
unset ltblue
unset ltgreen
unset brown


if [[ "${TERM}" == "dumb" ]]; then

    echo "" > /dev/null

else

    # check if stdout is a terminal...
    if test -t 1; then

        # see if it supports colors...
        ncolors=$(tput colors)

        if test -n "$ncolors" && test $ncolors -ge 8; then
            export bold="$(tput bold)"
            export underline="$(tput smul)"
            export standout="$(tput smso)"
            export standout_end="$(tput rmso)"
            export normal="$(tput sgr0)"
            export nocolor="$(tput sgr0)"
            export nc="$(tput sgr0)"
            export dim="$(tput dim)"
            export black="$(tput setaf 0)"
            export red="$(tput setaf 1)"
            export green="$(tput setaf 2)"
            export yellow="$(tput setaf 3)"
            export blue="$(tput setaf 4)"
            export magenta="$(tput setaf 5)"
            export cyan="$(tput setaf 6)"
            export ice="$(tput setaf 43)"
            export white="$(tput setaf 7)"
            export orange="$(tput setaf 166)"
            export purple="$(color 135)"
            export gray="$(color 240)"
            export ltred="$(color 203)"
            export ltblue="$(color 32)"
            export ltgreen="$(color 42)"
            export brown="$(color 130)"
        fi
    fi
fi




