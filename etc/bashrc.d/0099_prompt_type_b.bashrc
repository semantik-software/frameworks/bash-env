#!/usr/bin/env bash
#scriptpath=$(dirname  ${BASH_SOURCE:?})
scriptpathabs=$(realpath ${BASH_SOURCE:?})
scriptpath=$(dirname ${scriptpathabs})
scriptname=$(basename ${BASH_SOURCE:?})
loadenv_notify ${scriptname}

# source git-prompt
source ${scriptpath:?}/git-prompt.sh

export GIT_PS1_STATESEPARATOR=" "
export GIT_PS1_SHOWUPSTREAM="auto"         # auto, verbose, name, Legacy, git, svn
export GIT_PS1_SHOWUNTRACKEDFILES="1"      # Show untracked files (empty == no)
export GIT_PS1_DESCRIBE_STYLE="default"    # contains, branch, describe, default
export GIT_PS1_SHOWDIRTYSTATE="1"          # nonempty=true / empty=don't show
export GIT_PS1_SHOWCOLORHINTS=""           # empty == disable

# setting git-prompt parameters
export GIT_PS1_SHOWDIRTYSTATE=true
export GIT_PS1_SHOWUPSTREAM="auto verbose name git"
export GIT_PS1_SHOWSTASHSTATE=true
export GIT_PS1_SHOWUNTRACKEDFILES=true
export GIT_PS1_STATESEPARATOR=" "
export GIT_PS1_COMPRESSSPARSESTATE=true
export GIT_PS1_SHOWCONFLICTSTATE=true
export GIT_PS1_HIDE_IF_PWD_IGNORED=true

# Custom hostname
if [[ $OSTYPE == 'darwin'* ]]; then
    MYHOSTNAME=${HOST:?}
else
    MYHOSTNAME=$(hostnamectl | grep 'Static hostname' | cut -d':' -f2)
fi

#git_branch() {
#    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ \1 /'
#}

battery_status() {
    echo "$(batman check --raw 2> /dev/null | cut -d' ' -f3)"
}

function virtualenv_info(){
    local venv=""
    # Get Virtual Env
    if [[ -n "$VIRTUAL_ENV" ]]; then
        # Strip out the path and just leave the env name
        venv="${VIRTUAL_ENV##*/}"
        #venv="(`basename \"$VIRTUAL_ENV\"`)"
    fi
    [[ -n "$venv" ]] && echo "${blue}(venv${normal}:${magenta}$venv${blue})${normal} "
}
export VIRTUAL_ENV_DISABLE_PROMPT=1


export VENV="\$(virtualenv_info)";
PS_VENV="${VENV}"

PS_SEP="\[$(color 148)\]|\[${normal}\]"
PS_PRE="\[${ltgreen}\]\j\[${normal}\] "
PS_HOST="\[${green}\]\u\[${normal}\]@\[${purple}\]${HOST}\[${normal}\] ${PS_SEP}"
PS_BATT="\$(battery_status)"
PS_CWD="\[${ltblue}\]\w\[${normal}\] ${PS_SEP}"
# todo: hide separator if we're not in a git repo
PS_GIT="\[${ice}\]\$(__git_ps1 '(%s)')\[${normal}\]"

PS1="\n${debian_chroot:+($debian_chroot)}${PS_HOST} ${PS_BATT}${PS_CWD} ${PS_GIT}\n${PS_PRE}${PS_VENV}\$ "

