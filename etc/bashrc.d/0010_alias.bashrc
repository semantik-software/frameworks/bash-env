#!/usr/bin/env bash
set +x
scriptdir=$(dirname $BASH_SOURCE)
scriptname=$(basename $BASH_SOURCE)
loadenv_notify ${scriptname}


#------------------------------------------------------
# Alias configuration
#------------------------------------------------------
opts_common_ls=(
    -hF
    --color=always
    #--group-directories-first
)
alias ll="ls -ltr ${opts_common_ls[@]}"
alias ls="ls -1 ${opts_common_ls[@]}"
alias lln="ls -l ${opts_common_ls[@]}"

alias lsexec='find . -maxdepth 1 -perm -111 -type f'
alias h='history'
alias where='which'
alias csv2table='column -t -s","'
alias env='env | sort'
alias less='less -R'
alias sshpw='ssh -o PubKeyAuthentication=no -o PreferredAuthentications=password'
alias grep='grep --color=always'

# In Windows WSL, running `code` might not work
if [ -e "/mnt/c/Users/wcmcl/AppData/Local/Programs/Microsoft VS Code/bin/code" ]; then
    alias code="/mnt/c/Users/wcmcl/AppData/Local/Programs/Microsoft\ VS\ Code/bin/code"
fi


# OSX things
if [ "$(uname)" == "Darwin" ]; then
    if [[ -e "/Applications/Wing Pro 9.app" ]]; then
    # if [[ -e "/Applications/Wing\ Pro.app" ]]; then
        alias wing9='open -n /Applications/Wing\ Pro\ 9.app --args'
        loadenv_notify_indent1 "create wing9 alias"
    fi
    if [[ -e "/Applications/Wing Pro 10.app" ]]; then
        alias wing10='open -n /Applications/Wing\ Pro\ 10.app --args'
        loadenv_notify_indent1 "create wing10 alias"
    fi
    if [[ -e "/Applications/SourceTree.app" ]]; then
        alias sourcetree='open -n /Applications/SourceTree.app --args'
    fi
fi






# reset verbosity
set +x

