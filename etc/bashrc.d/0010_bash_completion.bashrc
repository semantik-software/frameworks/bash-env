#!/usr/bin/env bash
scriptpath=$(dirname  ${BASH_SOURCE:?})
scriptname=$(basename ${BASH_SOURCE:?})
loadenv_notify ${scriptname}

complete -df rm
complete -d  rmdir
complete -df ls
complete -df ll
complete -df ln
complete -df mv
complete -df cp
complete -d  cd
complete -f  vim
complete -f  less
complete -f  cat
complete -f  more
complete -df scp
complete -df rsync
complete -fd tar
complete -fd zip
complete -fd bzip2
complete -fd gzip
complete -fd git
complete -fd chmod
complete -fd chgrp
complete -fd make
complete -fd gmake
complete -fd ninja
complete -fd cmake
complete -fd ccmake
complete -fd source
complete -df yum
complete -df mkdir
complete -df tail
complete -f  rpm
complete -f  rpmbuild
complete -f  yum
complete -fd sudo
complete -fd time
complete -fd nohup
complete -fd gcc
complete -fd g++
complete -fd mpicc
complete -fd mpic++
complete -fd mpicxx
complete -fd mpirun
complete -fd emacs



# This is from /etc/bash.bashrc:
# enable bash completion in interactive shells
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'



