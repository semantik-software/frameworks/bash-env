#!/usr/bin/env bash
scriptdir=$(dirname $BASH_SOURCE)
scriptname=$(basename $BASH_SOURCE)
loadenv_notify ${scriptname}

#------------------------------------------------------
# LS_COLORS configuration for directory listings
#------------------------------------------------------
#
# http://linux-sxs.org/housekeeping/lscolors.html
#
# http://askubuntu.com/questions/466198/how-do-i-change-the-color-for-directories-with-ls-in-the-console
#
unset LS_COLORS
unset CLICOLOR
unset LSCOLORS


# define some colors for use in LS_COLORS
function define_lscolors() {
    export lscolor_red="0;31"
    export lscolor_green="0;32"
    export lscolor_yellow="0;33"
    export lscolor_blue="0;34"
    export lscolor_purple="0;35"
    export lscolor_cyan="0;36"
    export lscolor_white="1;37"
    export lscolor_normal="0;38"
    export lscolor_darkgray="0;90"
    export lscolor_darkergray="0;92"
    export lscolor_dimwhite="0;95"
}

function undefine_lscolors() {
    unset lscolor_red
    unset lscolor_green
    unset lscolor_yellow
    unset lscolor_blue
    unset lscolor_purple
    unset lscolor_cyan
    unset lscolor_white
    unset lscolor_normal
    unset lscolor_darkgray
    unset lscolor_darkergray
    unset lscolor_dimwhite
}

#if [[ "$PLATFORM" == 'darwin' ]]; then

# directory listing coloring for default LS in darwin
export CLICOLOR=1
export LSCOLORS="GxFxCxDxBxegedabagaced"

#else

# Coloring for Linux or OSX (with coreutils installed)
# 0;30 = black
# 0;31 = red
# 0;32 = green
# 0;33 = yellow
# 0;34 = blue
# 0;35 = purple
# 0;36 = cyan
# 1;37 = white
# 0;38 = normal?
# 0;90 = dark gray
# 0;92 = darker gray
# 0;95 = dim white

define_lscolors

# Define the colors for different file types
exec="${lscolor_red}"
dirs="${lscolor_cyan}"
zips="${lscolor_purple}"
sources="${lscolor_yellow}"
#sourceobj="${lscolor_blue}"
sourceobj="${lscolor_purple}"
dimfile="${lscolor_normal}"
# text_files="${lscolor_white}"
text_files=${lscolor_green}
image_files="${lscolor_green}"
bashrc="${lscolor_cyan}"


# Set up the LS_COLORS environment variable
LS_COLORS="di=${dirs}:fi=0;0:ln=0;35:pi=5:so=5:bd=5:cd=5:or=5;33;40:mi=0;0:ex=${exec}"
LS_COLORS="${LS_COLORS}:*.zip=${zips}:*.tgz=${zips}:*.gz=${zips}:*.tar.gz=${zips}:*.bz2=${zips}"
LS_COLORS="${LS_COLORS}:*.rs=${sources}:${LS_COLORS}:*.py=${sources}:*.cpp=${sources}:*.hpp=${sources}:*.h=${sources}:*.groovy=${sources}"
LS_COLORS="${LS_COLORS}:*.jenkinsfile=${source}"
LS_COLORS="${LS_COLORS}:*.pyc=${sourceobj}:*.pyo=${sourceobj}:*.o=${sourceobj}"
LS_COLORS="${LS_COLORS}:*.md=${text_files}:*.txt=${text_files}"
LS_COLORS="${LS_COLORS}:*.bashrc=${bashrc}:*.sh=${bashrc}:*.bash=${bashrc}"
LS_COLORS="${LS_COLORS}:*.bash.x=${dimfile}:*.bashrc.x=${dimfile}"
LS_COLORS="${LS_COLORS}:*.jpg=${image_files}:*.png=${image_files}:*.xcf=${image_files}"
export LS_COLORS

#fi

undefine_lscolors
