#!/usr/bin/env bash
scriptpath=$(dirname  ${BASH_SOURCE:?})
scriptname=$(basename ${BASH_SOURCE:?})
loadenv_notify ${scriptname}


if [[ -f "/etc/profile.d/lmod.sh" ]]; then
    source /etc/profile.d/lmod.sh
fi

#envvar_prepend_or_create MODULEPATH ${HOME}/etc/modulefiles
envvar_set_or_create MODULEPATH ${HOME}/etc/modulefiles
