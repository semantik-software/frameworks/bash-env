#!/usr/bin/env bash
scriptpath=$(dirname  ${BASH_SOURCE:?})
scriptname=$(basename ${BASH_SOURCE:?})
loadenv_notify ${scriptname}



# executable_exists
#
# param1: executable (with path if necessary)
function executable_exists()
{
    local cmd=${1:?}
    local output=1
    if [ ! command -v ${cmd:?} &> /dev/null ]; then
        output=0
    fi
    echo ${output:?}
}
export -f executable_exists


# execute_command
#
# param1: command to execute
function execute_command()
{
    local command=${1:?}
    message_std "$ ${magenta}${command:?}${normal}"

    local is_executable=$(executable_exists ${command:?})

    if [[ "${is_executable}" == "1" ]]; then

        if [[ -z ${DRYRUN} ]]; then
            local _start=`date +%s`
            eval ${command:?}
            local err=$?
            local _stop=`date +%s`
            local _runtime=$((_stop-_start))
            if [ $err -ne 0 ]; then
                message_std "${red}FAILED $(color 100)(${_runtime} s)${normal}"
            else
                message_std "${green}OK $(color 100)(${_runtime} s)${normal}"
            fi
        else
            message_std "${yellow}DRYRUN (command not executed)${normal}"
        fi

    else
        message_std "${red}ERROR: command '${command:?}' is not executable"
        message_std "${red}FAILED${normal}"
    fi
    return $err
}
export -f execute_command


# execute_command_checked
#
# param1: command to execute
function execute_command_checked()
{
    local command=${1:?}
    message_std "$ ${magenta}${command:?}${normal}"

    local is_executable=$(executable_exists ${command:?})

    if [[ "${is_executable}" == "1" ]]; then

        if [[ -z ${DRYRUN} ]]; then
            local _start=`date +%s`
            eval ${command:?}
            local err=$?
            local _stop=`date +%s`
            local _runtime=$((_stop-_start))

            if [ $err -ne 0 ]; then
                message_failure
                exit $err
            else
                echo -e "${green}OK $(color 100)(${_runtime} s)${normal}"
            fi

        else
            message_std "${yellow}DRYRUN (command not executed)${normal}"
        fi
    else
        print_message "${red}ERROR: command '${command:?}' is not executable"
        print_message "${red}FAILED${normal}"
        exit 32
    fi
}
export -f execute_command_checked


