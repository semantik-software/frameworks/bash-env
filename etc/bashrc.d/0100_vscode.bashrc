#!/usr/bin/env bash
scriptpath=$(dirname  ${BASH_SOURCE:?})
scriptname=$(basename ${BASH_SOURCE:?})
loadenv_notify ${scriptname}

if [[ "x${TERM_PROGRAM}" == "xvscode" ]]; then
    echo -e "${green}VSCODE Terminal${normal}"
fi

