#!/usr/bin/env bash --posix
scriptdir=$(dirname $BASH_SOURCE)
scriptname=$(basename $BASH_SOURCE)
loadenv_notify ${scriptname}

# set +x
# . ${scriptdir}/common.bash


# ----------------------

HOMEBREW_INSTALL_PREFIX="/opt/homebrew"

#------------------------------------------------------
# Functions
#------------------------------------------------------

function toupper() {
    local text=${1:?}
    echo "$text" | tr '[:lower:]' '[:upper:]'
}


function brew_get_installed_packages() {
    local random32=$(random_string_32)
    local brew_list_file="/tmp/brew_list_${random32}.txt"
    if [ ! -e ${brew_list_file:?} ]; then
        #brew list --formula -1 > ${brew_list_file:?}
        brew list -1 > ${brew_list_file:?}
    fi
    echo ${brew_list_file:?}
}


function brew_package_is_installed() {
    local package_name=${1:?}

    local package_name_caps=$(toupper ${package_name})
    package_name_caps=${package_name_caps/-/_}
    #package_name_caps=$(echo $package_name_caps | sed "s/-/_/")

    unset HOMEBREW_${package_name_caps}_INSTALLED

    if [[ "${HOMEBREW_INSTALLED}" == "YES" ]]; then
        package_name_caps="${package_name_caps//\@/_}"
        package_name_caps="${package_name_caps//\./_}"
        if [ $(string_exists_in_file ${brew_list_file:?} "^${package_name}$") -eq 1 ]; then
            loadenv_notify_indent1 "Homebrew package ${magenta}${package_name} ${green}FOUND"
            envvar_set_or_create HOMEBREW_${package_name_caps}_INSTALLED "YES"
            return "1"
        else
            loadenv_notify_indent1 "Homebrew package ${magenta}${package_name} ${red}NOT FOUND"
            envvar_set_or_create HOMEBREW_${package_name_caps}_INSTALLED "NO"
            return "0"
        fi
    fi
}


function brew_package_is_installed_with_version() {
    local package_name=${1:?}
    local package_vers=${2:?}

    local package_name_caps=$(toupper ${package_name})
    package_name_caps=${package_name_caps/-/_}

    unset HOMEBREW_${package_name_caps}_INSTALLED
    unset HOMEBREW_${package_name_caps}_VERS_INST

    # set envvar for PREFERRED version (may not actually get it)
    envvar_set_or_create HOMEBREW_${package_name_caps:?}_VERS_PREF "${package_vers:?}"

    if [[ "${HOMEBREW_INSTALLED}" == "YES" ]]; then
        if [ $(string_exists_in_file ${brew_list_file:?} "^${package_name:?}@${package_vers:?}$") -eq 1 ]; then
            loadenv_notify_indent1 "Homebrew package ${magenta}${package_name}${blue}@${cyan}${package_vers:?} ${green}FOUND"
            envvar_set_or_create HOMEBREW_${package_name_caps:?}_INSTALLED "YES"
            envvar_set_or_create HOMEBREW_${package_name_caps:?}_VERS_INST "${package_vers:?}"
        fi
    fi
}


function configure_homebrew() {
    unset HOMEBREW_INSTALLED
    unset HOMEBREW_EDITOR
    unset HOMEBREW_PREFIX_PATH
    if [[ -e ${HOMEBREW_INSTALL_PREFIX}/bin/brew ]]; then
        loadenv_notify_indent1 "Package ${magenta}homebrew${blue} is ${green}INSTALLED"
        envvar_set_or_create HOMEBREW_INSTALLED "YES"
        envvar_set_or_create HOMEBREW_PREFIX_PATH $(${HOMEBREW_INSTALL_PREFIX}/bin/brew --prefix)
        envvar_prepend_or_create PATH "${HOMEBREW_PREFIX_PATH}/bin:${HOMEBREW_PREFIX_PATH}/sbin"
        envvar_prepend_or_create MANPATH "${HOMEBREW_PREFIX_PATH}/manpages:${HOMEBREW_PREFIX_PATH}/share/man"
        envvar_prepend_or_create DYLD_LIBRARY_PATH "${HOMEBREW_PREFIX_PATH}/lib"
        envvar_prepend_or_create LD_LIBRARY_PATH "${HOMEBREW_PREFIX_PATH}/lib"
        if [ -z ${HOMEBREW_GITHUB_API_TOKEN} ]; then
            loadenv_notify_indent1 "${red}ERROR${blue}: missing ${magenta}HOMEBREW_GITHUB_API_TOKEN ${blue}envvar."
        fi
        envvar_set_or_create HOMEBREW_EDITOR vim
    else
        loadenv_notify_indent1 "${magenta}Homebrew${blue} is ${red}NOT INSTALLED"
        envvar_set_or_create HOMEBREW_INSTALLED "NO"
    fi
}


#------------------------------------------------------
# HOMEBREW configurations
#------------------------------------------------------


# unset environment variables that might get set in this script
unset HOMEBERW_COREUTILS_INSTALLED
unset HOMEBREW_AUTOSSH_INSTALLED

# If darwin, is homebrew installed?
if [[ "$platform" == "darwin" ]]; then
    loadenv_notify_indent1 "Platform is ${magenta}Darwin"

    export HOMEBREW_GITHUB_API_TOKEN="4b9075c432b46e2068f337790243befa5211e560"
    configure_homebrew

    if [[ "$HOMEBREW_INSTALLED" == "YES" ]]; then

        # Get a list of installed packages
        brew_list_file=$(brew_get_installed_packages)

        brew_package_is_installed 'coreutils'
        envvar_remove_path_entry PATH    "${HOMEBREW_PREFIX_PATH}/opt/coreutils/libexec/gnubin"
        envvar_remove_path_entry MANPATH "${HOMEBRWE_PREFIX_PATH}/opt/coreutils/share/man"
        if [[ ${HOMEBREW_COREUTILS_INSTALLED} == "YES" ]]; then
            #envvar_prepend_or_create PATH    "/usr/local/opt/coreutils/libexec/gnubin"
            #envvar_prepend_or_create MANPATH "/usr/local/opt/coreutils/share/man"
            envvar_prepend_or_create PATH    "${HOMEBREW_PREFIX_PATH}/opt/coreutils/libexec/gnubin"
            envvar_prepend_or_create MANPATH "${HOMEBREW_PREFIX_PATH}/opt/coreutils/share/man"
        fi

        brew_package_is_installed 'autossh'
        if [[ ${HOMEBREW_AUTOSSH_INSTALLED} == "YES" ]]; then
            envvar_set_or_create AUTOSSH_PORT 0
            envvar_set_or_create AUTOSSH_POLL 300
            alias ssh="autossh -o ServerAliveInterval=60"
        #else
        #    unset AUTOSSH_PORT
        #    unset AUTOSSH_POLL
        fi

        brew_package_is_installed 'ncurses'
        envvar_remove_path_entry PATH      "${HOMEBREW_PREFIX_PATH}/opt/ncurses/bin"
        envvar_remove_path_entry LDFLAGS   "-L${HOMEBREW_PREFIX_PATH}/opt/ncurses/lib"
        envvar_remove_path_entry CPPFLAGS  "-I${HOMEBREW_PREFIX_PATH}/opt/ncurses/include"
        if [[ ${HOMEBREW_NCURSES_INSTALLED} == "YES" ]]; then
            envvar_prepend_or_create PATH      "${HOMEBREW_PREFIX_PATH}/opt/ncurses/bin"
            envvar_prepend_or_create LDFLAGS   "-L${HOMEBREW_PREFIX_PATH}/opt/ncurses/lib"
            envvar_prepend_or_create CPPFLAGS  "-I${HOMEBREW_PREFIX_PATH}/opt/ncurses/include"
        fi

        brew_package_is_installed 'qt'
        envvar_remove_path_entry LDFLAGS   "-L${HOMEBREW_PREFIX_PATH}/opt/qt/lib"
        envvar_remove_path_entry CPPFLAGS  "-I${HOMEBREW_PREFIX_PATH}/opt/qt/include"
        envvar_remove_path_entry  PKG_CONFIG_PATH "${HOMEBREW_PREFIX_PATH}/opt/qt/lib/pkgconfig"
        if [[ ${HOMEBREW_QT_INSTALLED} == "YES" ]]; then
            envvar_prepend_or_create LDFLAGS   "-L${HOMEBREW_PREFIX_PATH}/opt/qt/lib"
            envvar_prepend_or_create CPPFLAGS  "-I${HOMEBREW_PREFIX_PATH}/opt/qt/include"
            envvar_prepend_or_create PKG_CONFIG_PATH "${HOMEBREW_PREFIX_PATH}/opt/qt/lib/pkgconfig"
        fi

        brew_package_is_installed 'readline'
        envvar_remove_path_entry LDFLAGS         "-L${HOMEBREW_PREFIX_PATH}/opt/readline/lib"
        envvar_remove_path_entry CPPFLAGS        "-I${HOMEBREW_PREFIX_PATH}/opt/readline/include"
        envvar_remove_path_entry PKG_CONFIG_PATH "${HOMEBREW_PREFIX_PATH}/opt/readline/lib/pkgconfig"
        if [[ ${HOMEBREW_READLINE_INSTALLED} == "YES" ]]; then
            envvar_prepend_or_create LDFLAGS         "-L${HOMEBREW_PREFIX_PATH}/opt/readline/lib"
            envvar_prepend_or_create CPPFLAGS        "-I${HOMEBREW_PREFIX_PATH}/opt/readline/include"
            envvar_prepend_or_create PKG_CONFIG_PATH "${HOMEBREW_PREFIX_PATH}/opt/readline/lib/pkgconfig"
        fi


        brew_package_is_installed 'zip'
        envvar_remove_path_entry PATH "${HOMEBREW_PREFIX_PATH}/opt/zip/bin"
        envvar_remove_path_entry MANPATH "${HOMEBREW_PREFIX_PATH}/opt/zip/share/man"
        if [[ ${HOMEBREW_ZIP_INSTALLED} == "YES" ]]; then
            #envvar_prepend_or_create PATH "/usr/local/opt/zip/bin"
            envvar_prepend_or_create PATH "${HOMEBREW_PREFIX_PATH}/opt/zip/bin"
            envvar_prepend_or_create MANPATH "${HOMEBREW_PREFIX_PATH}/opt/zip/share/man"
        fi

        brew_package_is_installed 'zlib'
        envvar_remove_path_entry LDFLAGS         "-L${HOMEBREW_PREFIX_PATH}/opt/zlib/lib"
        envvar_remove_path_entry CPPFLAGS        "-I${HOMEBREW_PREFIX_PATH}/opt/zlib/include"
        envvar_remove_path_entry PKG_CONFIG_PATH "${HOMEBREW_PREFIX_PATH}/opt/zlib/lib/pkgconfig"
        envvar_remove_path_entry MANPATH         "${HOMEBREW_PREFIX_PATH}/opt/zlib/share/man"
        if [[ ${HOMEBREW_ZLIB_INSTALLED} == "YES" ]]; then
            envvar_prepend_or_create LDFLAGS         "-L${HOMEBREW_PREFIX_PATH}/opt/zlib/lib"
            envvar_prepend_or_create CPPFLAGS        "-I${HOMEBREW_PREFIX_PATH}/opt/zlib/include"
            envvar_prepend_or_create PKG_CONFIG_PATH "${HOMEBREW_PREFIX_PATH}/opt/zlib/lib/pkgconfig"
            envvar_prepend_or_create MANPATH         "${HOMEBREW_PREFIX_PATH}/opt/zlib/share/man"
        fi

        brew_package_is_installed 'unzip'
        envvar_remove_path_entry PATH "${HOMEBREW_PREFIX_PATH:?}/opt/unzip/bin"
        if [[ ${HOMEBREW_UNZIP_INSTALLED} == "YES" ]]; then
            envvar_prepend_or_create PATH "${HOMEBREW_PREFIX_PATH:?}/opt/unzip/bin"
        fi

        brew_package_is_installed 'source-highlight'
        if [[ ${HOMEBREW_SOURCE_HIGHLIGHT_INSTALLED} == "YES" ]]; then
            LESSPIPE=`which src-hilite-lesspipe.sh`
            export LESSOPEN="| ${LESSPIPE} %s"
            export LESS=' -R '
            # Add -X (leave text on terminal when less closes)
            #     -F (exit less if the output fits on one screen)
        fi


        brew_package_is_installed 'java'
        envvar_remove_path_entry PATH     "${HOMEBREW_PREFIX_PATH}/opt/java/bin"
        envvar_remove_path_entry CPPFLAGS "-I${HOMEBREW_PREFIX_PATH}/opt/java/include"
        if [[ ${HOMEBREW_JAVA_INSTALLED} == "YES" ]]; then
            envvar_prepend_or_create PATH     "${HOMEBREW_PREFIX_PATH}/opt/java/bin"
            envvar_prepend_or_create CPPFLAGS "-I${HOMEBREW_PREFIX_PATH}/opt/java/include"
        fi


        brew_package_is_installed 'openjdk'
        envvar_remove_path_entry PATH     "${HOMEBREW_PREFIX_PATH}/opt/openjdk/bin"
        envvar_remove_path_entry CPPFLAGS "-I${HOMEBREW_PREFIX_PATH}/opt/openjdk/include"
        if [[ ${HOMEBREW_OPENJDK_INSTALLED} == "YES" ]]; then
            envvar_prepend_or_create PATH     "${HOMEBREW_PREFIX_PATH}/opt/openjdk/bin"
            envvar_prepend_or_create CPPFLAGS "-I${HOMEBREW_PREFIX_PATH}/opt/openjdk/include"
            envvar_set_or_create JAVA_HOME "${HOMEBREW_PREFIX_PATH}/opt/openjdk/"
        fi


        brew_package_is_installed 'curl'
        envvar_remove_path_entry PATH     "${HOMEBREW_PREFIX_PATH}/opt/curl/bin"
        envvar_remove_path_entry LDFLAGS  "-L${HOMEBREW_PREFIX_PATH}/opt/curl/lib"
        envvar_remove_path_entry CPPFLAGS "-I${HOMEBREW_PREFIX_PATH}/opt/java/include"
        if [[ ${HOMEBREW_CURL_INSTALLED} == "YES" ]]; then
            envvar_prepend_or_create PATH     "${HOMEBREW_PREFIX_PATH}/opt/curl/bin"
            envvar_prepend_or_create LDFLAGS  "-L${HOMEBREW_PREFIX_PATH}/opt/curl/lib"
            envvar_prepend_or_create CPPFLAGS "-I${HOMEBREW_PREFIX_PATH}/opt/java/include"
        fi


        brew_package_is_installed_with_version "python" "3.8"
        brew_package_is_installed_with_version "python" "3.10"
        brew_package_is_installed_with_version "python" "3.11"
        brew_package_is_installed_with_version "python" "3.12"
        if [[ ${HOMEBREW_PYTHON_INSTALLED} == "YES" && ${HOMEBREW_PYTHON_VERS_INST} == "${HOMEBREW_PYTHON_VERS_PREF}" ]]; then
            unset PYTHONPATH
            # Homebrew should install python<version> in the bin dir
            #envvar_prepend_or_create PATH   "/usr/local/opt/python\@${python_version:?}/bin"
            #envvar_set_or_create PYTHONPATH "${HOMEBREW_PREFIX_PATH:?}/lib/python3.8/site-packages"
            #envvar_set_or_create PYTHONPATH "/usr/local/lib/python#{python_version:?}/"
            unset HOMEBREW_PYTHON_VERS_PREF
        fi


        brew_package_is_installed 'pyenv'
        if [[ ${HOMEBREW_PYENV_INSTALLED} == "YES" ]]; then
            #envvar_prepend_or_create PATH "$HOME/.pyenv/versions/3.8.18/bin"
            #envvar_prepend_or_create PATH "$HOME/.pyenv/shims"
            envvar_prepend_or_create PYENV_ROOT "$HOME/.pyenv"
            #[[ -d $PYENV_ROOT/bin ]] && envvar_prepend_or_create PATH "$PYENV_ROOT/bin"
            #eval "$(pyenv init -)"
            [[ -d $PYENV_ROOT/shims ]] && envvar_prepend_or_create PATH "$PYENV_ROOT/shims"
            eval "$(pyenv init -)"
            loadenv_notify_indent1 "${magenta}Use${cyan} \$ pyenv versions ${magenta}to list available versions."
            loadenv_notify_indent1 "${magenta}Use${cyan} \$ pyenv local 3.8 ${magenta}to set version for current dir."
        fi


        #brew_package_is_installed 'openssl@3'
        brew_package_is_installed_with_version "openssl" "3"
        envvar_remove_path_entry PATH            "${HOMEBREW_PREFIX_PATH}/opt/openssl@${HOMEBREW_OPENSSL_INST_PREF}/bin"
        envvar_remove_path_entry CPPFLAGS        "-I${HOMEBREW_PREFIX_PATH}/opt/openssl@${HOMEBREW_OPENSSL_INST_PREF}/include"
        envvar_remove_path_entry LDFLAGS         "-L${HOMEBREW_PREFIX_PATH}/opt/openssl@${HOMEBREW_OPENSSL_INST_PREF}/lib"
        envvar_remove_path_entry PKG_CONFIG_PATH "-L${HOMEBREW_PREFIX_PATH}/opt/openssl@${HOMEBREW_OPENSSL_INST_PREF}/lib"
        if [[ ${HOMEBREW_OPENSSL_INSTALLED} == "YES" && ${HOMEBREW_OPENSSL_VERS_INST} == "${HOMEBREW_OPENSSL_INST_PREF}" ]]; then
            envvar_prepend_or_create PATH            "${HOMEBREW_PREFIX_PATH}/opt/openssl@${HOMEBREW_OPENSSL_VERS_INST}/bin"
            envvar_prepend_or_create CPPFLAGS        "-I${HOMEBREW_PREFIX_PATH}/opt/openssl@${HOMEBREW_OPENSSL_VERS_INST}/include"
            envvar_prepend_or_create LDFLAGS         "-L${HOMEBREW_PREFIX_PATH}/opt/openssl@${HOMEBREW_OPENSSL_VERS_INST}/lib"
            envvar_prepend_or_create PKG_CONFIG_PATH "-L${HOMEBREW_PREFIX_PATH}/opt/openssl@${HOMEBREW_OPENSSL_VERS_INST}/lib"
            unset HOMEBREW_OPENSSL_VERS_PREF
        fi


        brew_package_is_installed 'sqlite'
        envvar_remove_path_entry PATH            "${HOMEBREW_PREFIX_PATH}/opt/sqlite/bin"
        envvar_remove_path_entry CPPFLAGS        "-I${HOMEBREW_PREFIX_PATH}/opt/sqlite/include"
        envvar_remove_path_entry LDFLAGS         "-L${HOMEBREW_PREFIX_PATH}/opt/sqlite/lib"
        envvar_remove_path_entry PKG_CONFIG_PATH "${HOMEBREW_PREFIX_PATH}/opt/sqlite/lib/pkgconfig"
        envvar_remove_path_entry MANPATH         "${HOMEBREW_PREFIX_PATH}/opt/sqlite/share/man"
        if [[ ${HOMEBREW_SQLITE_INSTALLED} == "YES" ]]; then
            envvar_prepend_or_create PATH            "${HOMEBREW_PREFIX_PATH}/opt/sqlite/bin"
            envvar_prepend_or_create CPPFLAGS        "-I${HOMEBREW_PREFIX_PATH}/opt/sqlite/include"
            envvar_prepend_or_create LDFLAGS         "-L${HOMEBREW_PREFIX_PATH}/opt/sqlite/lib"
            envvar_prepend_or_create PKG_CONFIG_PATH "${HOMEBREW_PREFIX_PATH}/opt/sqlite/lib/pkgconfig"
            envvar_prepend_or_create MANPATH         "${HOMEBREW_PREFIX_PATH}/opt/sqlite/share/man"
        fi


        brew_package_is_installed 'groovy'
        if [[ ${HOMEBREW_GROOVY_INSTALLED} == "YES" ]]; then
            envvar_prepend_or_create GROOVY_HOME "${HOMEBREW_PREFIX_PATH}/opt/groovy/libexec"
        fi

        brew_package_is_installed 'node'
        brew_package_is_installed 'yarn'
        brew_package_is_installed 'multipass'
        brew_package_is_installed 'inetutils'

        # if source-highlight is installed, add a colorized less function.
        # To get the package:
        #    $ brew install source-highlight
        #
        #if [ -e "/usr/local/bin/src-hilite-lesspipe.sh" ]; then
        #    function lesscolor() {
        #        /usr/local/bin/src-hilite-lesspipe.sh $1 | less -R -m
        #    }
        #fi

        brew_package_is_installed_with_version "gcc" "9"
        if [[ ${HOMEBREW_GCC_INSTALLED} == "YES" && ${HOMEBREW_GCC_VERS_INST} == "${HOMEBREW_GCC_VERS_PREF}" ]]; then
            unset HOMEBREW_GCC_VERS_PREF
            envvar_set_or_create CC   "/usr/local/bin/gcc-${HOMEBREW_GCC_VERS_INST}"
            envvar_set_or_create CXX  "/usr/local/bin/g++-${HOMEBREW_GCC_VERS_INST}"
            envvar_set_or_create CPP  "/usr/local/bin/cpp-${HOMEBREW_GCC_VERS_INST}"
            envvar_set_or_create LD   "/usr/local/bin/gcc-${HOMEBREW_GCC_VERS_INST}"
            envvar_set_or_create GCOV "/usr/local/bin/gcov-${HOMEBREW_GCC_VERS_INST}"
            envvar_set_or_create FC   "/usr/local/bin/gfortran-${HOMEBREW_GCC_VERS_INST}"
            alias  c++=${CXX}
            alias  g++=${CXX}
            alias  gcc=${CC}
            alias  cpp=${CPP}
            alias  ld=${CC}
            alias  cc=${CC}
            alias  fc=${FC}
            alias  gcov=${GCOV}
        fi


        brew_package_is_installed 'ruby'
        envvar_remove_path_entry LDFLAGS  "-L${HOMEBREW_PREFIX_PATH}/opt/ruby/lib"
        envvar_remove_path_entry CPPFLAGS "-I${HOMEBREW_PREFIX_PATH}/opt/ruby/include"
        envvar_remove_path_entry PATH     "${HOMEBREW_PREFIX_PATH}/opt/ruby/bin"
        if [[ ${HOMEBREW_LLVM_INSTALLED} == "YES" ]]; then
            envvar_prepend_or_create LDFLAGS  "-L${HOMEBREW_PREFIX_PATH}/opt/ruby/lib"
            envvar_prepend_or_create CPPFLAGS "-I${HOMEBREW_PREFIX_PATH}/opt/ruby/include"
            envvar_prepend_or_create PATH     "${HOMEBREW_PREFIX_PATH}/opt/ruby/bin"
        fi


        brew_package_is_installed 'llvm'
        if [[ ${HOMEBREW_LLVM_INSTALLED} == "YES" ]]; then
            envvar_prepend_or_create LDFLAGS  "-L/usr/local/opt/llvm/lib"
            envvar_prepend_or_create CPPFLAGS "-I/usr/local/opt/llvm/include"
            envvar_prepend_or_create PATH     "/usr/local/opt/llvm/bin"
        fi


        brew_package_is_installed 'lmod'
        if [[ ${HOMEBREW_LMOD_INSTALLED} == "YES" ]]; then
            [ -f "${HOMEBREW_PREFIX_PATH:?}/opt/lmod/init/profile" ] && \
                source ${HOMEBREW_PREFIX_PATH}/opt/lmod/init/profile
        fi


        brew_package_is_installed 'iterm2'
        if [[ ${HOMEBREW_LMOD_INSTALLED} == "YES" ]]; then
            alias iterm2='open -a "iTerm"'
        fi


        #brew_package_is_installed 'postgresql@13'
        brew_package_is_installed_with_version "postgresql" "13"
        envvar_remove_path_entry LDFLAGS "-L${HOMEBREW_PREFIX_PATH:?}/opt/postgresql@${HOMEBREW_POSTGRESQL_VERS_PREF}/lib"
        envvar_remove_path_entry CPPFLAGS "-I/${HOMEBREW_PREFIX_PATH:?}/opt/postgresql@${HOMEBREW_POSTGRESQL_VERS_PREF}/include"
        envvar_remove_path_entry PATH "${HOMEBREW_PREFIX_PATH:?}/opt/postgresql@${HOMEBREW_POSTGRESQL_VERS_PREF}/bin"
        if [[ ${HOMEBREW_POSTGRESQL_INSTALLED} == "YES" && ${HOMEBREW_POSTGRESQL_VERS_INST} == "${HOMEBREW_POSTGRESQL_VERS_PREF}" ]]; then
            unset HOMEBREW_POSTGRESQL_VERS_PREF
            envvar_prepend_or_create LDFLAGS "-L${HOMEBREW_PREFIX_PATH:?}/opt/postgresql@${HOMEBREW_POSTGRESQL_VERS_INST}/lib"
            envvar_prepend_or_create CPPFLAGS "-I/${HOMEBREW_PREFIX_PATH:?}/opt/postgresql@${HOMEBREW_POSTGRESQL_VERS_INST}/include"
            envvar_prepend_or_create PATH "${HOMEBREW_PREFIX_PATH:?}/opt/postgresql@${HOMEBREW_POSTGRESQL_VERS_INST}/bin"
            #export LDFLAGS="-L/opt/homebrew/opt/postgresql@13/lib"
            #export CPPFLAGS="-I/opt/homebrew/opt/postgresql@13/include"
            #echo 'export PATH="/opt/homebrew/opt/postgresql@13/bin:$PATH"' >> /Users/williammclendon/.bash_profile
            #export PKG_CONFIG_PATH="/opt/homebrew/opt/postgresql@13/lib/pkgconfig"
            #brew services start postgresql@13
            alias postgresql_database="${HOMEBREW_PREFIX_PATH:?}/opt/postgresql@13/bin/postgres -D ${HOMEBREW_PREFIX_PATH:?}/var/postgresql@13"
        fi


        brew_package_is_installed 'redis@6.2'
        if [[ ${HOMEBREW_REDIS_6_2_INSTALLED} == "YES" ]]; then
            envvar_prepend_or_create PATH "${HOMEBREW_PREFIX_PATH:?}/opt/redis@6.2/bin"
            # brew services start redis@6.2
            # brew services stop redis@6.2
            # ${HOMEBREW_PREFIX_PATH:?}/opt/redis@6.2/bin/redis-server ${HOMEBREW_PREFIX_PATH:?}/etc/redis.conf
        fi


        brew_package_is_installed 'gnu-sed'
        envvar_remove_path_entry PATH "${HOMEBREW_PREFIX_PATH:?}/opt/gnu-sed/libexec/gnubin"
        if [[ ${HOMEBREW_GNU_SED_INSTALLED} == "YES" ]]; then
            envvar_prepend_or_create PATH "${HOMEBREW_PREFIX_PATH:?}/opt/gnu-sed/libexec/gnubin"
            envvar_set_or_create GNU_SED "gsed"
        fi


        brew_package_is_installed 'lapack'
        envvar_remove_path_entry LDFLAGS  "-L${HOMEBREW_PREFIX_PATH}/opt/lapack/lib"
        envvar_remove_path_entry CPPFLAGS "-I${HOMEBREW_PREFIX_PATH}/opt/lapack/include"
        envvar_remove_path_entry PKG_CONFIG_PATH "${HOMEBREW_PREFIX_PATH}/opt/lapack/lib/pkgconfig"
        if [[ ${HOMEBREW_LAPACK_INSTALLED} == "YES" ]]; then
        #if [ -e "/usr/local/opt/lapack" ]; then
            envvar_prepend_or_create LDFLAGS  "-L${HOMEBREW_PREFIX_PATH}/opt/lapack/lib"
            envvar_prepend_or_create CPPFLAGS "-I${HOMEBREW_PREFIX_PATH}/opt/lapack/include"
            envvar_prepend_or_create PKG_CONFIG_PATH "${HOMEBREW_PREFIX_PATH}/opt/lapack/lib/pkgconfig"
        fi


        # Note: I don't think there is a libLAS formula in homebrew
        #brew_package_is_installed 'liblas'
        #if [[ ${HOMEBREW_LIBLAS_INSTALLED} == "YES" ]]; then
        #if [ -e "/usr/local/opt/liblas" ]; then
        #    loadenv_notify_indent1 "liblas ${green}FOUND"
        #    envvar_prepend_or_create LDFLAGS  "-L${HOMEBREW_PREFIX_PATH}/opt/liblas/lib"
        #    envvar_prepend_or_create CPPFLAGS "-I${HOMEBREW_PREFIX_PATH}/opt/liblas/include"
        #    envvar_prepend_or_create PATH     "${HOMEBREW_PREFIX_PATH}/opt/liblas/bin"
        #fi


        #if [ -e "/usr/local/opt/libpq" ]; then
        brew_package_is_installed 'libpq'
        envvar_remove_path_entry LDFLAGS  "-L${HOMEBREW_PREFIX_PATH:?}/opt/libpq/lib"
        envvar_remove_path_entry CPPFLAGS "-I${HOMEBREW_PREFIX_PATH:?}/opt/libpq/include"
        envvar_remove_path_entry PATH "${HOMEBREW_PREFIX_PATH:?}/opt/libpq/bin"
        envvar_remove_path_entry PKG_CONFIG_PATH "${HOMEBREW_PREFIX_PATH:?}/opt/libpq/lib/pkgconfig"
        if [[ ${HOMEBREW_LIBPQ_INSTALLED} == "YES" ]]; then
            #loadenv_notify_indent1 "libpq ${green}FOUND"
            envvar_prepend_or_create LDFLAGS  "-L${HOMEBREW_PREFIX_PATH:?}/opt/libpq/lib"
            envvar_prepend_or_create CPPFLAGS "-I${HOMEBREW_PREFIX_PATH:?}/opt/libpq/include"
            envvar_prepend_or_create PATH "${HOMEBREW_PREFIX_PATH:?}/opt/libpq/bin"
            envvar_prepend_or_create PKG_CONFIG_PATH "${HOMEBREW_PREFIX_PATH:?}/opt/libpq/lib/pkgconfig"
        fi


        brew_package_is_installed 'netbeans'
        if [[ ${HOMEBREW_NETBEANS_INSTALLED} == "YES" ]]; then
            alias netbeans="open /Applications/Apache\\ NetBeans.app --args --jdkhome /opt/homebrew/opt/openjdk/"
        fi

        # Packages that just need checking but no actions
        brew_package_is_installed 'libmagic'
        brew_package_is_installed 'pango'
        brew_package_is_installed 'cairo'

        brew_package_is_installed 'vagrant'

        brew_package_is_installed 'rsync'

        # Remove the temp file in 5 minutes
        ((sleep 300; rm ${brew_list_file} > /dev/null 2>&1) &)
    fi      # if homebrew is installed
fi


unset HOMEBREW_INSTALL_PREFIX

# set +x

