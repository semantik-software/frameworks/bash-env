#!/usr/bin/env bash
# This is just a helper script that will load all the bashrc files in the
# etc/bashrc.d directory matching the pattern "????_*.bashrc" in lexicographical
# order. This is useful for testing out new bashrc files without having to
# manually source them in the main bashrc file.
#

shopt -s dotglob
shopt -s nullglob

FILES=$(find . -name "????_*.bashrc" -print0 | xargs -0 ls)

for f in $FILES
do
    source $f
done

export DOCKER_HELPER_LOADENV=1
