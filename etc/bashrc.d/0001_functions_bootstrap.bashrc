#!/usr/bin/env bash
#scriptpath=$(dirname  ${BASH_SOURCE:?})
scriptpath=$(realpath ${BASH_SOURCE:?})
scriptname=$(basename ${BASH_SOURCE:?})
scriptdir=$(dirname ${scriptpath})

#----[ BEGIN BOOTSTRAPPING ]----------
color_loadmsg=${green}
color_version=${green}
color_notify=${ice}
color_warning=${red}
color_envvar=${purple}
color_value=${red}


bashrc_notify() {
    printmsg=0
    if [ ! -n "$SSH_CLIENT" ] && [ ! -n "$SSH_TTY" ]; then
        printmsg=1
    fi
    if [[ "$printmsg" == "1" ]]; then
        scriptname=$(basename ${BASH_SOURCE:?})
        echo -e "$1"
    fi
}
export -f bashrc_notify


loadenv_notify() {
    local _msg=${1:?}
    bashrc_notify "${color_loadmsg}LOADENV${normal}: ${color_notify}${_msg:?}${normal}"
}
export -f loadenv_notify


loadenv_notify_indent1() {
    local _msg=${1:?}
    bashrc_notify "    > ${blue}${_msg:?}${normal}"
}
export -f loadenv_notify

#----[ END BOOTSTRAPPING ]----------

loadenv_notify ${scriptname}

export BASHRCDPATH=${scriptdir:?}
