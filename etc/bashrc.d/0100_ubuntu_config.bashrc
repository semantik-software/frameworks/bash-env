#!/usr/bin/env bash --posix
scriptdir=$(dirname $BASH_SOURCE)
scriptname=$(basename $BASH_SOURCE)
loadenv_notify ${scriptname}


#------------------------------------------------------
# Functions
#------------------------------------------------------

function is_ubuntu() {
    if [[ -f /etc/lsb-release ]]; then
        return 0
    else
        return 1
    fi
}

# Function to check if flatpak is installed
function flatpak_is_installed() {
    if command -v flatpak &> /dev/null; then
        return 1
    else
        return 0
    fi
}

# Function to check if a package is installed in Flatpak
function flatpak_package_is_installed() {
    local package=${1:?}
    if flatpak_is_installed; then
        if flatpak list | grep -q ${package}; then
            return 1
        fi
    fi
    return 0
}


#------------------------------------------------------
# Ubuntu configurations
#------------------------------------------------------

# check if this is an ubuntu system
if is_ubuntu; then
    loadenv_notify_indent1 "Configuring Ubuntu system"
    if [[ -f /etc/os-release ]]; then
        source /etc/os-release
        loadenv_notify_indent1 "OS: ${magenta}${PRETTY_NAME}"
    fi
    export -f is_ubuntu
    export -f flatpak_is_installed
    export -f flatpak_package_is_installed
else
    loadenv_notify_indent1 "Ubuntu ${red}not detected"
    return
fi


# Set up Google Chrome alias if it was installed via Flatpak
# https://ubuntuhandbook.org/index.php/2024/04/install-google-chrome-ubuntu-24-04-lts/
if flatpak_package_is_installed "Google Chrome"; then
    loadenv_notify_indent1 "Google Chrome ${green}is installed${blue} via Flatpak"
    alias google-chrome="flatpak run com.google.Chrome"
fi
