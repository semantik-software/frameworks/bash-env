#!/usr/bin/env bash
scriptpath=$(dirname  ${BASH_SOURCE:?})
scriptname=$(basename ${BASH_SOURCE:?})
loadenv_notify ${scriptname}


#---------------------
# some settings
#---------------------

# umask o-w     # sets 0002
# umask o-rwx     # sets 0007
umask 0002

ulimit -S -c 0     # no coredumps
set -o notify
set -o noclobber
set -o ignoreeof

# bind up and down arrows to history search functions
bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=2000
HISTFILESIZE=4000
HISTTIMEFORMAT='%F  %T > '

# get platform
platform='unknown'
unamestr=`uname -s`
if [[ "$unamestr" == 'Linux' ]]; then
    platform='linux'
elif [[ "$unamestr" == 'Darwin' ]]; then
    platform='darwin'
elif [[ "$unamestr" == 'FreeBSD' ]]; then
    platform='freebsd'
elif [[ "$unamestr" == 'MINGW64_NT-6.1' ]]; then
    platform='win7'
fi
export PLATFORM=$platform


# get hostname (without domain)
host=`hostname | cut -d"." -f1`
export HOST=$host


# Set up GCC colors
export GCC_COLORS='error=31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=36'


# Setup bash time (/usr/bin/time) format
export TIME="\nElapsed Time: %e (seconds)\n- CPU-seconds in kernel mode: %S\n- CPU-seconds in user mode  : %U"
alias time=/usr/bin/time

# Set up Subversion Environment Variables
# export SVN_EDITOR=vim

# Update PATH
envvar_prepend_or_create PATH /usr/local/bin
envvar_prepend_or_create PATH $HOME/bin
envvar_prepend_or_create PATH $HOME/.local/bin

# Localization
# export LC_ALL="en_US.UTF-8"

# Pager
# Can set this to `cat` or `less` if wanted.
export SYSTEMD_PAGER=""


# OpenMP Settings
export OMP_PROC_BIND=spread
export OMP_PLACES=threads


# reset verbosity
set +x

