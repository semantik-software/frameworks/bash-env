#!/usr/bin/env bash
scriptdir=$(dirname $BASH_SOURCE)
scriptname=$(basename $BASH_SOURCE)
loadenv_notify ${scriptname}

# ----------------------

# for git-prompt (if using it to replace parse_git_branch() with git-prompt.sh)
#export GIT_PS1_STATESEPARATOR=" "
#export GIT_PS1_SHOWUPSTREAM="auto"         # auto, verbose, name, Legacy, git, svn
#export GIT_PS1_SHOWUNTRACKEDFILES="1"      # Show untracked files (empty == no)
#export GIT_PS1_DESCRIBE_STYLE="default"    # contains, branch, describe, default
#export GIT_PS1_SHOWDIRTYSTATE="1"          # nonempty=true / empty=don't show
#export GIT_PS1_SHOWCOLORHINTS="1"

# A two-line colored Bash prompt (PS1) with Git branch and a line decoration
# which adjusts automatically to the width of the terminal.
# Recognizes and shows Git, SVN and Fossil branch/revision.
# Screenshot: http://img194.imageshack.us/img194/2154/twolineprompt.png
# Michal Kottman, 2012
#
# Source: https://gist.github.com/mkottman/1936195

PS_LINE=`printf -- '- %.0s' {1..300}`

function virtualenv_info(){
    local venv=""
    # Get Virtual Env
    if [[ -n "$VIRTUAL_ENV" ]]; then
        # Strip out the path and just leave the env name
        venv="${VIRTUAL_ENV##*/}"
        #venv="(`basename \"$VIRTUAL_ENV\"`)"
    fi
    [[ -n "$venv" ]] && echo "${blue}(venv${normal}:${magenta}$venv${blue})${normal} "
}
export VIRTUAL_ENV_DISABLE_PROMPT=1


function parse_git_branch {
  PS_BRANCH=''
  #PS_FILL="${PS_LINE:0:$COLUMNS}"
  if [ -d .svn ]; then
    PS_BRANCH="(svn r$(svn info|awk '/Revision/{print $2}'))"
    return
  elif [ -f _FOSSIL_ -o -f .fslckout ]; then
    PS_BRANCH="(fossil $(fossil status|awk '/tags/{print $2}')) "
    return
  fi
  ref=$(git symbolic-ref HEAD 2> /dev/null) || return
  PS_BRANCH="(git ${ref#refs/heads/}) "
}
PROMPT_COMMAND=parse_git_branch


PS_PRE="\[${ltgreen}\]\j\[${normal}\] "
# PS_PRE="$(expr `dirs -v | wc -l` - 1) ${ltblue}\j${normal} "

#PS_PATH="\w"
#PS_PATH='$(pwd | sed -E -e "s|^$HOME|~|" -e '\''s|^([^/]*/[^/]*/).*(/[^/]*)|\1..\2|'\'')'
PS_PATH='$(pwd|awk -F/ -v "n=\[$(tput cols)\]" -v "h=^$HOME" '\''{sub(h,"~");n=0.35*n;b=$1"/"$2} length($0)<=n || NF==3 {print;next;} NF>3{b=b"/../"; e=$NF; n-=length(b $NF); for (i=NF-1;i>3 && n>length(e)+1;i--) e=$i"/"e;} {print b e;}'\'')'

PS_INFO="\[${red}\]\u@\h\[${normal}\]:\[${yellow}\]${PS_PATH}\[${normal}\]"

PS_GIT="\[${purple}\]\${PS_BRANCH}\[${normal}\]"

PS_CTHULHU=""
#PS_CTHULHU="${green}/|\\(${red}*${green}>,,,<${red}*${green})/|\\ ${normal}\n"

export VENV="\$(virtualenv_info)";
PS_VENV="${VENV}"

PS_TIME="\[\033[\$((COLUMNS-10))G\]\[${normal}\] [\[${yellow}\]\@\[${normal}\]]"

PS_TIME=""
PS_FILL=""

#export PS1="\n${PS_CTHULHU}${ltblue}\${PS_FILL}\[\033[0G\]${PS_INFO} ${PS_GIT}${PS_TIME}\n${PS_PRE}${PS_VENV}${normal}\$ "
export PS1="${ltblue}${PS_INFO} ${PS_VENV}${PS_GIT}${PS_TIME}\n${PS_PRE}\[${normal}\]\$ "

set +x

