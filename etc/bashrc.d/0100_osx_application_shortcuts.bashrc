#!/usr/bin/env bash --posix
scriptdir=$(dirname $BASH_SOURCE)
scriptname=$(basename $BASH_SOURCE)
loadenv_notify ${scriptname}

# set +x
# . ${scriptdir}/common.bash


if [[ "$platform" == "darwin" ]]; then

    #------------------------------------------------------
    # Functions
    #------------------------------------------------------

    # param 1: executable
    #function is_installed() {
    #    local executable=${1:?}
    #    which -s ${executable:?}
    #    if [[ $? == 0 ]]; then
    #        echo "1"
    #    else
    #        echo "0"
    #    fi
    #}
    #export -f is_installed

    function code () {
        VSCODE_CWD="$PWD" open -n -b "com.microsoft.VSCode" --args $* ;
    }
    export -f code

    function notify() {
        # Usage: notify "message" ["sound"]
        # Sound options: Basso, Blow, Bottle, Frog, Funk, Glass, Hero, Morse, Ping, Pop, Purr, Sosumi, Submarine, Tink
        local message=${1:-"Ding!"}
        local sound=${2:-"Glass"}

        echo -e "${cyan}Notification: ${message}${reset}"
        osascript -e "display notification \"${message:?}\" with title \"ALERT\" sound name \"${sound:?}\""
    }
    export -f notify

    function notify_sound_options() {
        echo "Notify Sound Options:"
        local sounds=(
            "Basso"
            "Blow"
            "Bottle"
            "Frog"
            "Funk"
            "Glass"
            "Hero"
            "Morse"
            "Ping"
            "Pop"
            "Purr"
            "Sosumi"
            "Submarine"
            "Tink"
        )
        for sound in ${sounds[@]}; do
            echo -n ">>> ${magenta}${sound} "
            for i in {1..3}; do
                echo -n "."
                afplay -v 1 "/System/Library/Sounds/${sound}.aiff"
                sleep 0.5
            done
            echo -e "${reset}"
        done
    }
    export -f notify_sound_options

    #----------------------------------------------------
    # Paths
    #----------------------------------------------------

    # For Python3.9
    SYSTEM_PYTHONLIB_BIN_DIR="${HOME}/Library/Python/3.9/bin"
    if [[ -d ${SYSTEM_PYTHONLIB_BIN_DIR} ]]; then
        envvar_remove_path_entry PATH ${SYSTEM_PYTHONLIB_BIN_DIR}
        envvar_append_or_create PATH ${SYSTEM_PYTHONLIB_BIN_DIR}
    fi

fi
