#!/usr/bin/env bash
scriptpath=$(dirname  ${BASH_SOURCE:?})
scriptname=$(basename ${BASH_SOURCE:?})
loadenv_notify ${scriptname}


# -----------------------------------
#   C O M M O N   U T I L I T I E S
# -----------------------------------

# message_std
#
# param1: prefix
# param2: message text
function message_std
{
    local prefix=${1}
    local message=${2}
    local postfix=${3}
    printf "${prefix}${message}\n" >& /dev/stdout
}
export -f message_std


# message
#
# param1: message text
function message
{
    local message=${1}
    printf "${cyan}>>> ${yellow}${message}${nocolor}\n" >& /dev/stdout
}

# message_warn
#
# param1: prefix
# param2: message text
function message_warn
{
    local prefix=${1}
    local message=${2}
    local postfix=${3}
    printf "${prefix}${red}[WARNING]${normal}${message}\n" >& /dev/stdout
}
export -f message_warn


# message_std_nocrlf
#
# param1: prefix
# param2: message text
function message_std_nocrlf
{
    local prefix=${1}
    local message=${2}
    local postfix=${3}
    printf "${prefix}${message}" >& /dev/stdout
}
export -f message_std_nocrlf


#
# message_success
#
function message_success()
{
    echo -e "${green}==============================${normal}" >& /dev/stdout
    echo -e "${green}=          SUCCESS!${normal}" >& /dev/stdout
    echo -e "${green}==============================${normal}" >& /dev/stdout
}
export -f message_success


#
# message_failure
#
function message_failure()
{
    echo -e "${red}==============================${normal}" >& /dev/stdout
    echo -e "${red}=          FAILURE!${normal}" >& /dev/stdout
    echo -e "${red}==============================${normal}" >& /dev/stdout
}
export -f message_failure


# print_centered_text
#
# Prints out a centered text string with endcaps
#
# param1: width
# param2: endcaps
# param3: text to print
function print_centered_text()
{
    local width=${1:?}
    local endcap=${2:?}
    local text=${3:?}
    local textsize=${#text}
    local capsize=${#endcap}
    local span1=$((($width + $textsize - $capsize * 2)/2))
    local span2=$(($width - $span1 - $capsize * 2))
    printf "%s%${span1}s%${span2}s%s\n" "${endcap}" "${text}" "" "${endcap}" >& /dev/stdout
}
export -f print_centered_text


# message_banner()
#
# Prints out a banner block with date/time stamp.
#
# param1: banner text to print
function message_banner()
{
    local banner_text=${1:?}
    local textdate=$(date +"%Y-%m-%d %H:%M:%S")
    local width=60
    echo -e "" >& /dev/stdout
    echo -e "+----------------------------------------------------------+" >& /dev/stdout
    print_centered_text ${width} "|" "${banner_text}"
    print_centered_text ${width} "|" " "
    print_centered_text ${width} "|" "${textdate}"
    echo -e "+----------------------------------------------------------+" >& /dev/stdout
}
export -f message_banner


# message_banner_2lines()
#
# Prints out a two line banner plus a date/time stamp.
# param1: banner text line 1
# param2: banner text line 2
function message_banner_2lines()
{
    local banner_text_line1=${1:?}
    local banner_text_line2=${2:?}
    local textdate=$(date +"%Y-%m-%d %H:%M:%S")
    local width=60
    echo -e "" >& /dev/stdout
    echo -e "+----------------------------------------------------------+" >& /dev/stdout
    print_centered_text ${width} "|" "${banner_text_line1}"
    print_centered_text ${width} "|" "${banner_text_line2}"
    print_centered_text ${width} "|" " "
    print_centered_text ${width} "|" "${textdate}"
    echo -e "+----------------------------------------------------------+" >& /dev/stdout
}
export -f message_banner_2lines



# print_envvar
#
# Prints out an envvar in a nicely formatted way.
# param1: Name of the envvar to print
unset print_envvar
function print_envvar()
{
    local varname=${1:?}
    #echo "varname = ${varname}" >&2
    #echo "!varname= ${!varname}" >&2

    if [ ! -v "${varname}" ]; then
        echo -e "${cyan}ENVVAR: ${magenta}${varname} ${ltblue}= ${red}!!IS UNDEFINED!!${normal}"
    elif [ -z "${!varname}" ]; then
        echo -e "${cyan}ENVVAR: ${magenta}${varname} ${ltblue}= ${yellow}!!IS EMPTY!!${normal}"
    else
        echo -e "${cyan}ENVVAR: ${magenta}${varname} ${ltblue}= ${magenta}${!varname}${normal}"
    fi
}
export -f print_envvar





