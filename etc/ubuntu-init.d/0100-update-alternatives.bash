#!/usr/bin/env bash
#
# Configure update-alternatives
#
#

#
# Install update-alternatives
#
sudo apt-get install update-alternatives

#
# Python3
#
sudo update-alternatives --remove-all python
sudo update-alternatives --install /usr/local/bin/python python /usr/bin/python3.8 8
sudo update-alternatives --install /usr/local/bin/python python /usr/bin/python3.10 10
# sudo update-alternatives --install /usr/local/bin/python python /usr/bin/python3.11 11
# sudo update-alternatives --install /usr/local/bin/python python /usr/bin/python3.12 12

sudo update-alternatives --remove-all python3
sudo update-alternatives --install /usr/local/bin/python3 python3 /usr/bin/python3.8 8
sudo update-alternatives --install /usr/local/bin/python3 python3 /usr/bin/python3.10 10
# sudo update-alternatives --install /usr/local/bin/python3 python3 /usr/bin/python3.11 11
# sudo update-alternatives --install /usr/local/bin/python3 python3 /usr/bin/python3.12 12

sudo update-alternatives --config python
sudo update-alternatives --config python3

