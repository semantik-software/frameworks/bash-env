#!/usr/bin/env bash
#
# Installs Python 3.12
#
#

export VERSION=3.12

#
# Update APT packages
#
sudo apt-get update

#
# Install Python3
#
execute_command_checked "sudo apt-get install -y python${VERSION:?}"

#
# Install important packages
#
execute_command "sudo apt-get install -y python${VERSION:?}-stdlib"
execute_command "sudo apt-get install -y python${VERSION:?}-doc"
execute_command "sudo apt-get install -y python${VERSION:?}-distutils"
execute_command "sudo apt-get install -y python${VERSION:?}-venv"
execute_command "sudo apt-get install -y python${VERSION:?}-numpy"

