#!/usr/bin/env bash
#
# Bootstrap script
#
scriptpath=$(dirname  ${BASH_SOURCE:?})
scriptname=$(basename ${BASH_SOURCE:?})


# -----------------------------------------------------
# Sources all *.bashrc files in $HOME/etc/bashrc.d in
# sorted order by filename.
# param1: path to bashrc.d folder containing files conforming to
#         the naming convention of ????_*.bashrc
# -----------------------------------------------------
function bashrc-load() {
    local DIR_BASHRC_D=${1:?}

    if [ -d ${DIR_BASHRC_D:?} ]; then
        #shopt -s dotglob
        #shopt -s nullglob
        local FILES=${DIR_BASHRC_D:?}/????_*.bashrc

        for file in ${FILES}; do
            source ${file:?}
        done
    fi
}



