-- -*- lua -*-
--------------------------------------------------------
-- See: https://lmod.readthedocs.io/en/latest/
-- See: https://lmod.readthedocs.io/en/latest/050_lua_modulefiles.html
--
--------------------------------------------------------

help([[
Python 3.8.18
]])

-- Local Variables
local version  = "3.8.18"
local homedir  = os.getenv("HOME")
local basepath = pathJoin(homedir, "opt/python3.8.18")

-- whatis description
whatis("Python 3.8.18 Interpreter")
whatis("URL: https://www.python.org")

-- Path stuff
prepend_path("PATH", pathJoin(basepath, "bin"))

-- environment Variables
--setenv("TEST_ENVVAR_LMOD_BAR","2.0")

-- Note: pushenv saves the previous state of the envvar
pushenv("PYTHONPATH", pathJoin(basepath, "lib", "python3.8"))
-- $HOME/opt/python3.8/lib/python3.8

set_alias("python","python3.8")

-- Set a family for this module
-- >>> only one module per family is allowed to be loaded
--     at any given time.
family("python")



