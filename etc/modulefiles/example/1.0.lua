-- -*- lua -*-
--------------------------------------------------------
-- See: https://lmod.readthedocs.io/en/latest/
-- See: https://lmod.readthedocs.io/en/latest/050_lua_modulefiles.html
--
--------------------------------------------------------

help([[
Help message
]])

-- Local Variables
local version  = "1.0"
local basepath = "/usr"

-- whatis description
whatis("Test module")
whatis("URL: www.acme.org")

-- Path stuff
prepend_path("TEST_ENVVAR_PATH", pathJoin(base, "local", "bin"))
prepend_path("TEST_ENVVAR_PATH", pathJoin(base, "local", "lib"))

-- environment Variables
setenv("TEST_ENVVAR_LMOD_BAR","2.0")

-- Note: pushenv saves the previous state of the envvar
pushenv("TEST_ENVVAR_LMOD_VER",version)
pushenv("TEST_ENVVAR_LMOD_BAZ", "4.0")

-- Set a family for this module
-- >>> only one module per family is allowed to be loaded
--     at any given time.
family("testenv")



