SSH Configuration
=================


### config
The config file lets us configure how we can use our SSH.

You may need to _add_ your ssh keys via:
```bash
ssh-add ~/.ssh/id_rsa
ssh-add ~/.ssh/id_rsa_work
ssh-add ~/.ssh/id_rsa_personal
```

Check what SSH keys are registered:
```bash
ssh-add -l
```

Delete all registered SSH keys
```bash
ssh-add -D
```

